\chapter{Generación de trayectoria} \label{cap:trayectoria}


Hasta aquí vimos cómo calcular los puntos de paso del cuerpo y las patas
calculados por el gait (capítulo \ref{cap:gaits}). El siguiente paso para
obtener la cinemática del robot móvil con patas es la generación de
trayectorias. Para generar una caminata recta se deben generar dos
trayectorias: (a) la trayectoria lineal del cuerpo entre dos puntos del
\emph{gait}, y (b) la trayectoria de las patas en fase de transferencia en cada
paso del \emph{gait}.

\section{Trayectoria del cuerpo}

Suponiendo que se quiere pasar por la $POSE_1$ y la $POSE_2$ (figura
\ref{fig:trayectoriaposes}):

\begin{figure}[ht]
  \centering
  \newsavebox{\trayectoriaposes}
  \begin{lrbox}{\trayectoriaposes}
		\input{imag/tex/trayectoriaposes.tex}
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\trayectoriaposes}}
	    \caption{Trayectoria lineal entre dos poses.}
		\label{fig:trayectoriaposes}
\end{figure}

\begin{eqnarray*}
    POSE_1 &=& \left[ \begin{array}{ccc|c}
      \multicolumn{3}{c|}{R_1} & \overline{p_{1}}\\ \hline
      0&0&0&1
      \end{array}\right]\\
    POSE_2 &=& \left[ \begin{array}{ccc|c}
      \multicolumn{3}{c|}{R_2} & \overline{p_{2}}\\ \hline
      0&0&0&1
      \end{array}\right]
\end{eqnarray*}

\noindent se puede escribir una respecto a la otra según:

\begin{equation*}
  POSE_1 D_{12} = POSE_2
\end{equation*}

Usando las propiedades de transformaciones que se explican en el apéndice
\ref{sec:trasfhomogenea} se calcula la matriz de transformación:

\begin{eqnarray*}
    D_{12} &=& POSE^{-1}_{1} POSE_2\\
           &=& \left[ \begin{array}{ccc|c}
               \multicolumn{3}{c|}{R^{T}_{1}} & -R^{T}_{1} \overline{p_{1}}\\ \hline
               0&0&0&1
               \end{array}\right] \left[ \begin{array}{ccc|c}
               \multicolumn{3}{c|}{R_2} & \overline{p_{2}}\\ \hline
               0&0&0&1
               \end{array}\right]\\
           &=& \left[ \begin{array}{ccc|c}
               \multicolumn{3}{c|}{R^{T}_{1} R_2} & R^{T}_{1} (\overline{p_{2}} - \overline{p_{1}})\\ \hline
               0&0&0&1
               \end{array}\right]
\end{eqnarray*}

\noindent La variación de orientación estará dada por una variación en el
tiempo de la matriz de rotación $R^{T}_{1} R_{2}(t)$ y la variación de posición
por $R^{T}_{1} (\overline{p_{2}}(t) - \overline{p_{1}})$. A continuación se
explica la forma de calcular los puntos intermedios del movimiento

\subsection{Posición}

Para generar la trayectoria lineal se utiliza una solución conocida de la
robótica clásica (Generación de trayectorias, capítulo 7, Craig \cite{craig}).

Se define una zona de aceleración alrededor de los puntos por los que tiene
que pasar el cuerpo. Dentro de esta zona el cuerpo acelerará constantemente y
luego hará un movimiento a velocidad constante hasta la siguiente zona de
aceleración (\ref{fig:trayectoriaacc}).

\begin{figure}[ht]
  \centering
  \newsavebox{\trayectoriaacc}
  \begin{lrbox}{\trayectoriaacc}
		\input{imag/tex/trayectoriaacc.tex}
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\trayectoriaacc}}
	  \caption{Trayectoria con zona de aceleración entre dos puntos}
		\label{fig:trayectoriaacc}
\end{figure}

Si se define $\overline{\Delta C} = \overline{p}_{C} - \overline{p}_{B}$ y
$\overline{\Delta A} = \overline{p}_{A} - \overline{p}_{B}$ la aceleración en
las tres coordenadas se escribe como:

\begin{align*}
\overline{a}_{cc} &= \frac{\Delta V}{\Delta T}\\
             &= \frac{\overline{v}(t_{acc}) - \overline{v}(-t_{acc})}{2 t_{acc}}
\end{align*}
\begin{equation*}
\left. \begin{array}{cc}
  \overline{v}(t_{acc}) &= \frac{\overline{\Delta C}}{T_1}\\
  \overline{v}(-t_{acc}) &= \frac{-\overline{\Delta A}}{t_{acc}}
  \end{array} \right\} \overline{a}_{cc} = \left( \frac{\overline{\Delta C}}{T_{1}} + \frac{\overline{\Delta A}}{t_{acc}} \right) \frac{1}{2 t_{acc}}
\end{equation*}

\noindent Integrando la aceleración se obtiene la velocidad:

\begin{equation*}
  \overline{v}(t) = \overline{a}_{cc} t + C
\end{equation*}

\noindent Evaluado en $-t_{acc}$ tiene que ser igual a la velocidad del tramo
anterior:

\begin{align*}
  \left.\overline{v}\right|_{t_{acc}} &= -\frac{\overline{\Delta A}}{t_{acc}}\\
  C &= \left( \frac{\overline{\Delta C}}{T_{1}} + \frac{\overline{\Delta A}}{t_{acc}} \right) \frac{1}{2} - \frac{\overline{\Delta A}}{t_{acc}}\\
  C &= \left( \frac{\overline{\Delta C}}{T_{1}} - \frac{\overline{\Delta A}}{t_{acc}} \right) \frac{1}{2}
\end{align*}
\begin{align*}
  \therefore \overline{v}(t) &= \overline{a}_{cc} t + C\\
     &= \left( \frac{\overline{\Delta C}}{T_{1}} + \frac{\overline{\Delta A}}{t_{acc}} \right) \frac{t}{2 t_{acc}} + \left( \frac{\overline{\Delta C}}{T_{1}} - \frac{\overline{\Delta A}}{t_{acc}} \right) \frac{1}{2}\\
     &= \frac{\overline{\Delta C}}{T_{1}} \frac{t + t_{acc}}{2 t_{acc}} + \frac{\overline{\Delta A}}{t_{acc}} \frac{t - t_{acc}}{2 t_{acc}}
\end{align*}

\noindent Integrando una vez más se obtiene la posición:

\begin{eqnarray*}
  \overline{r}(t) = \frac{\overline{\Delta C}}{T_{1}} \frac{(t + t_{acc})^2}{4 t_{acc}} + \frac{\overline{\Delta A}}{t_{acc}} \frac{(t - t_{acc})^2}{4 t_{acc}} + D
\end{eqnarray*}

\noindent Evaluado en $-t_{acc}$ tiene que ser igual al punto $A$:

\begin{align*}
  \overline{r}(-t_{acc}) = \overline{p}_{A} &= \frac{\overline{\Delta A}}{t_{acc}} \frac{(-t_{acc} - t_{acc})^2}{4 t_{acc}} + D\\
  &= \overline{\Delta A} + D = \overline{p}_{A} - \overline{p}_{B} + D\\
  & \therefore D = \overline{p}_{B}
\end{align*}

\noindent Obteniendo así la ecuación para el cálculo de posición del cuerpo en las zonas de aceleración:

\begin{eqnarray}
  \overline{r}(t) = \frac{\overline{\Delta C}}{T_{1}} \frac{(t + t_{acc})^2}{4 t_{acc}} + \frac{\overline{\Delta A}}{t_{acc}} \frac{(t - t_{acc})^2}{4 t_{acc}} + \overline{p}_{B}
  \label{eq:trayectoriacm}
\end{eqnarray}

\noindent que es valido para $-t_{acc} < t < t_{acc}$. Para la zona lineal se plantea un recta:

\begin{equation*}
  \overline{r}(t) = \frac{\overline{\Delta C}}{T_1} t + \overline{p}_{B}
\end{equation*}

\noindent que es valido para $t_{acc} < t < T_1 - t_{acc}$.

\subsection{Orientación}

Para la orientación se plantea la misma solución pero sobre los ángulos de
Euler equivalentes a la matriz de rotación de las pose (apéndice
\ref{sec:orientacion}). Para $POSE_1$ serán $(\phi_1, \theta_1, \psi_1)$, y
$(\phi_2, \theta_2, \psi_2)$ para $POSE_2$.

Se tiene entonces la ecuación sobre $\overline{\theta} = \left[
\begin{array}{ccc} \phi&\theta&\psi \end{array}\right]^{T}$ con $\phi =
\text{pitch}$, $\theta = \text{roll}$ y $\psi = \text{yaw}$:

\begin{eqnarray}
  \overline{\theta}(t) = \frac{\overline{\Delta C}}{T_{1}} \frac{(t + t_{acc})^2}{4 t_{acc}} + \frac{\overline{\Delta A}}{t_{acc}} \frac{(t - t_{acc})^2}{4 t_{acc}} + \overline{\theta}_{B}
  \label{eq:trayectoriaorientacion}
\end{eqnarray}

\noindent donde $\overline{\Delta C} = \theta_C - \theta_B$ y $\overline{\Delta A} = \theta_A - \theta_B$.

\section{Trayectoria de las patas en transferencia}

El movimiento de las patas en transferencia es un movimiento libre, lo único
que se necesita es que la pata esté despegada del piso en todo su movimiento.

Se propone utilizar la solución presentada anteriormente definiendo un punto de
paso intermedio a una altura elegida de forma arbitraria en el medio del
desplazamiento.

\begin{figure}[ht]
  \centering
  \newsavebox{\trayectoriapata}
  \begin{lrbox}{\trayectoriapata}
		\input{imag/tex/trayectoriapata.tex}
  \end{lrbox}
  \resizebox{0.6\textwidth}{!}{\usebox{\trayectoriapata}}
	    \caption{Trayectoria de una pata en transferencia.}
		\label{fig:trayectoriapata}
\end{figure}

Para sincronizar el movimiento del cuerpo con el movimiento de adelanto de las
patas todo el movimiento debe hacerse en el mismo tiempo $T_1$ en el que avanza
el cuerpo.
