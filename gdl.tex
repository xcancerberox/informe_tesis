\chapter{Estudio de grados de libertad}\label{cap:gdl}

El número de grados de libertad se refiere al número mínimo de parámetros que
es necesario especificar para determinar completamente el estado de una
estructura o mecanismo. El espacio donde desarrollará sus movimientos el robot,
tiene 6 grados de libertad (gdl), es decir, para especificar el estado del robot
son necesarias 6 variables, 3 para identificar la posición $(x,y,z)$ y 3 para
identificar la orientación: \emph{pitch}, \emph{yaw} y \emph{roll} (figura
\ref{fig:agilidad}).

\begin{figure}[h]
	\begin{center}
    \includegraphics[width=0.7\textwidth]{imag/eps/agilidad.pdf}
		\caption{Grados de libertad en un espacio de 3 dimensiones.}
		\label{fig:agilidad}
	\end{center}
\end{figure}

Los grados de libertad del robot estarán dados por sus vínculos y
articulaciones ({\bf cadena cinemática}). En el caso de la máquina caminante el
cálculo es complejo ya que las cadenas que lo conforman pueden estar
conectadas en un bucle cerrado (cada vínculo esta conectado a todos los otros
por más de un camino, \emph{closed-loop chain}), o no formar un bucle (los
vínculos se conectan por un único camino, \emph{open-loop chain})

Interesa estudiar los grados de libertad del mecanismo para
elegir la configuración óptima (cantidad de articulaciones por pata y cantidad
de patas). En general es posible derivar una expresión para el cálculo de los
grados de libertad a partir de los grados de libertad de cada articulación
(\cite{RobotAnalysisTsai}).

\section{Criterio de Grübler-Kutzbach}

Sabiendo el numero de grados de libertad del espacio de trabajo $\lambda$, y
suponiendo que todas las articulaciones están libres de restricciones, salvo
que uno de los links esta atado al piso, se puede calcular los grados de
libertad del mecanismo como $\lambda(n-1)$ (donde $n$ es la cantidad de
vínculos del mecanismo). Si se agregan las restricciones, $c_i$, de cada
mecanismo se puede calcular los grados de libertad $F$ del sistema como:

\begin{equation*}
  F = \lambda(n-1)-\sum^{j}_{i=1}c_i
\end{equation*}

\noindent donde $j$ es la cantidad de articulaciones del mecanismo, asumiendo
todas uniones unitarias, y $F$ los grados de libertad del mecanismo. La
ecuación se puede reescribir sabiendo que las libertades más las restricciones
de un vínculo son iguales a los grados de libertad del espacio de trabajo
$\lambda = c_i + f_i$, entonces

\begin{eqnarray*}
  \sum^{j}_{i=1}c_i = \sum^{j}_{i=1} (\lambda - f_i) = j\lambda - \sum^{j}_{i=1} f_i\nonumber\\
  \therefore F = \lambda (n-j-1) + \sum_{i=1}^{j} f_i \label{eq:computo_dof}
\end{eqnarray*}

\section{Aplicación del criterio}

Se aplicará el criterio a dos configuraciones distintas de patas y se probarán
distinta cantidad de patas apoyadas.

\subsection{Miembros de dos grados de libertad}

Se propone como caso inicial una estructura con 3 patas de 2 gdl cada una (se
utilizara la notacion S para articulaciones de 3 gdl y R para 1 gdl, como se
puede ver en la figura \ref{fig:esquema_pata_2dof}, quedando una estructura
SRR). Cada pata tiene dos articulaciones de 1 gdl ($j_1=6$) y una articulación
de 3 gdl ($j_3=3$), dando un total de nueve articulaciones unitarias ($j=9$).
Cada pata tiene 2 vínculos y al ser una cadena cerrada se le agregan dos
vínculos más correspondientes al suelo y a la plataforma ($n=6+2=8$).
Utilizando la ecuación \ref{eq:computo_dof} resulta en $F=3$ gdl. Este
resultado sirve para descartar la configuración del mecanismo ya que al ser
menores los gdl del sistema a los del espacio el robot no podrá hacer un
movimiento ágil.

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemapatadosdof}
  \begin{lrbox}{\esquemapatadosdof}
		\input{imag/tex/esquema_pata_2dof.tex} % croped
  \end{lrbox}
  \resizebox{0.35\textwidth}{!}{\usebox{\esquemapatadosdof}}
		\caption{Pata de 2 grados de libertad.}
		\label{fig:esquema_pata_2dof}
\end{figure}

Como mejora se plantea utilizar un miembro más contra el suelo.  Si se toma un
robot con 4 patas como las anteriores se tienen ocho articulaciones de 1 gdl
($j_1=8$) y cuatro articulaciones de 3 gdl ($j_3=4$), dando un total de doce
articulaciones $j=12$. Ahora se tendrán 8 vínculos de las patas más el suelo y
la plataforma ($n=8+2=10$) dando un total de diez vínculos. Utilizando
nuevamente la ecuación \ref{eq:computo_dof} resulta en $F=2$. El agregado de
una pata más al mecanismo al contrario de mejorar la cantidad de grados de
libertad lo empeora. Si se agrega una pata más del mismo tipo se verá que se
pierde un grado de libertad más. Así al llegar al hexapod completo con 6 patas
de 2 gdl cada una, se tendrá $j_1=6$, $j_3=12$, $j=18$ y $n=12+2=14$ resultando
en que el robot tiene $0$ grados de libertad.

\subsection{Miembros de tres grados de libertad}

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemapatatresdof}
  \begin{lrbox}{\esquemapatatresdof}
		\input{imag/tex/esquema_pata_3dof.tex} % croped
  \end{lrbox}
  \resizebox{0.35\textwidth}{!}{\usebox{\esquemapatatresdof}}
		\caption{Pata de 3 grados de libertad.}
		\label{fig:esquema_pata_3dof}
\end{figure}

La segunda configuración que se propone es un robot con 3 patas de 3 gdl (SRRR Fig.
\ref{fig:esquema_pata_3dof}). En este caso cada pata tiene tres articulaciones
de 1 gdl ($j_1=9$) y una articulación de 3 gdl ($j_3=3$), dando un total de
doce articulaciones ($j=12$). Cada pata tiene tres vínculos y sumando la
plataforma y el suelo se tienen once vínculos ($n=9+2=11$). Utilizando la
ecuación \ref{eq:computo_dof} resulta en $F=6$. En este caso el robot tiene los
mismos grados de libertad que el espacio en que trabaja teniendo total agilidad
en el movimiento.

La pregunta que queda responder es si en todo momento del \emph{gait}, sin importar
cuantas patas estén apoyadas, el robot mantendrá su agilidad. Para ver esto se
agregan de a una las patas y se calculan los grados de libertad.

Si se agrega una pata se tienen doce articulaciones de 1 gdl ($j_1=12$), cuatro
articulaciones de 3 gdl ($j_3=4$), dando un total de dieciseis articulaciones
($j=16$). Ahora la cantidad de links se incrementa en tres, dando un total de
catorce ($n=12+2=14$), resultando en $F=6$. No se modifica los grados de
libertad del mecanismo completo. 

Si se agregan más patas pasa lo mismo. Es más, se pueden agregar la cantidad de
patas que se desee sin modificar los grados de libertad del mecanismo completo
(siempre que sean del mismo tipo, SRRR) en el espacio de 6 gdl.  Si se suponen
$k$ patas SRRR se tendrán tres articulaciones de 1 gdl por pata ($j_1=3 k$) y
una articulación de 3 gdl por pata ($j_3=k$), dando un total de $j=4 k$
articulaciones. Cada pata aporta tres vínculos a los que se le suman el suelo y
la plataforma dando un total de $n=3 k+2$ vínculos.

\begin{eqnarray*}
	F &=& 6[(3 k +2 )- 4 k - 1] + 3 k + 3 k\\
	&=& 6(1 - k) + 6 k\\
	&=& 6
\end{eqnarray*}

\noindent quedando demostrado que el agregado de patas no cambia los grados de
libertad del robot para más de 4 patas. Como se explicará más adelante se
eligen 6 patas por que es la configuración que permite al robot mantener su
estabilidad a lo largo de todo el movimiento.

