#!/usr/bin/env python

from setuptools import find_packages
from setuptools import setup

setup(
        name='KCTools',
        version='0.1',
        description='Kinematic chains tools.',
        author='Joaquin de Andres',
        author_email='xcancerberox@gmail.com',
        packages=find_packages('src'),
        package_dir={'': 'src'},
        install_requires=['numpy', 'pyglet'],
        test_suite='tests'
        )
