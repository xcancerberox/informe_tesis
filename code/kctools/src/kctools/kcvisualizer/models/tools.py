import kctools.kcvisualizer.models.base as base_models


def model_from_description(description):
    ModelClass = getattr(base_models, description['type'])
    kcmodel = ModelClass(**description['parameters'])

    for child_description in description['children']:
        new_kcmodel = model_from_description(child_description)
        kcmodel.add_child(new_kcmodel)

    return kcmodel
