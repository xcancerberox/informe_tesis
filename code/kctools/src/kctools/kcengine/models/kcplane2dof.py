import numpy as np

import kctools.inverse_kinematics_solution as iks
from kctools.mylogging import logger


class KCPlane2DOF():
    """
    Kinematic chain with 2 rotational joints and 2 links
    Joint1DOF_Z(theta1)
        --> Link_X(l1)
            --> Fixed_rotation_X(90)
                --> Joint1DOF_Z(theta2)
                    --> Link_X(l2)
    """
    def __init__(self, l1, l2):
        self.l1 = l1
        self.l2 = l2

    def forward_kinematics(self, theta1, theta2):
        logger.info('KCPlane2DOF forward kinematics for angles {}, {}'.format(theta1, theta2))
        pose = np.array(
                [
                    [
                        np.cos(theta1 + theta2),
                        -np.sin(theta1 + theta2),
                        0,
                        self.l1*np.cos(theta1) + self.l2*np.cos(theta1+theta2),
                        ],
                    [
                        np.sin(theta1 + theta2),
                        np.cos(theta1 + theta2),
                        0,
                        self.l1*np.sin(theta1) + self.l2*np.sin(theta1+theta2)
                        ],
                    [
                        0,
                        0,
                        1,
                        0,
                        ],
                    [
                        0,
                        0,
                        0,
                        1,
                        ],
                    ]
                )

        return pose[0:4, 3].reshape(4,)

    def inverse_kinematics(self, endpoint):
        logger.info('KCPlane2DOF inverse kinematics for point {}'.format(endpoint))

        solution = iks.IKSolution([None], [None])
        cosTheta2 = self.cos_theta2(endpoint)

        if self.reachable(endpoint):
            sinTheta2 = np.sqrt(1 - cosTheta2**2)

            # Positive solution for sqrt
            theta2 = np.arctan2(sinTheta2, cosTheta2)

            k1 = cosTheta2 * self.l2 + self.l1
            k2 = sinTheta2 * self.l2
            theta1 = np.arctan2(endpoint[1], endpoint[0]) - np.arctan2(k2, k1)
            solution.positive = [theta1, theta2]

            # Negative solution for sqrt
            theta2 = np.arctan2(-sinTheta2, cosTheta2)

            k1 = cosTheta2 * self.l2 + self.l1
            k2 = -sinTheta2 * self.l2
            theta1 = np.arctan2(endpoint[1], endpoint[0]) - np.arctan2(k2, k1)
            solution.negative = [theta1, theta2]
        logger.info('KCPlane2DOF solutions {}'.format(solution))

        return solution

    def cos_theta2(self, endpoint):
        costheta2 = ((endpoint[0]**2 + endpoint[1]**2) - (self.l1**2 + self.l2**2))/(2*self.l1*self.l2)
        return round(costheta2, 7)

    def reachable(self, endpoint):
        if not np.isclose(endpoint[2], 0):
            logger.info('Endpoint not in the plane {}'.format(endpoint))
            return False

        max_reach = self.l1 + self.l2
        endpoint_module = np.sqrt(endpoint[0]**2 + endpoint[1]**2)

        if endpoint_module > max_reach:
            logger.info('Endpoint not reachable by module {} > max_reach {}'.format(endpoint_module, max_reach))
            return False

        cosTheta2 = self.cos_theta2(endpoint)
        if abs(cosTheta2) <= 1:
            return True

        logger.info('Endpoint not reachable {}'.format(endpoint))
        return False
