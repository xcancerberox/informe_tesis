import imp
import numpy as np
import sys
import time

import diloboderusabderus.body_trajectory_generator as tgen
from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.zmq_interface import RobotConfigurationPublisher
from diloboderusabderus.hexapod_model import Hexapod

pub = RobotConfigurationPublisher()
morphology_config = imp.load_source('', sys.argv[1])

body_height = (morphology_config.legs_lengths[1] + morphology_config.legs_lengths[2])*.7
position = np.array((0, 0, body_height, 1))
rotation = np.array((0, 0, 0, 1))

central_legs_point_position = [
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        np.array((
            morphology_config.legs_lengths[0] + morphology_config.legs_lengths[1]*.5,
            0,
            -body_height,
            1)),
        ]

central_config = RobotConfiguration(
                                body_position=position.copy(),
                                body_rotation=rotation.copy(),
                                support_pattern=[0, 1, 2, 3, 4, 5],
                                legs_point_position=central_legs_point_position
                                )
hexapod = Hexapod(position, rotation, morphology_config)

distance = 10
north_config = hexapod.configuration_to_new_body_pose(
                                        central_config.copy(),
                                        central_config.body_position.copy() + np.array((0, distance, 0, 0)),
                                        central_config.body_rotation.copy()
                                        )
south_config = hexapod.configuration_to_new_body_pose(
                                        central_config.copy(),
                                        central_config.body_position.copy() + np.array((0, -distance, 0, 0)),
                                        central_config.body_rotation.copy()
                                        )
east_config = hexapod.configuration_to_new_body_pose(
                                        central_config.copy(),
                                        central_config.body_position.copy() + np.array((distance, 0, 0, 0)),
                                        central_config.body_rotation.copy()
                                        )
west_config = hexapod.configuration_to_new_body_pose(
                                        central_config.copy(),
                                        central_config.body_position.copy() + np.array((-distance, 0, 0, 0)),
                                        central_config.body_rotation.copy()
                                        )


def send_linear_trajectory(initial_config, final_config, velocity):
    hexapod = Hexapod(initial_config.body_position.copy(), initial_config.body_rotation.copy(), morphology_config)
    to_send = initial_config.copy()

    for config in tgen.body_linear_trajectory(initial_config, final_config):
        to_send = hexapod.configuration_to_new_body_pose(
                                            initial_config.copy(),
                                            config.body_position,
                                            config.body_rotation)
        to_send.body_position = config.body_position
        to_send.body_rotation = config.body_rotation

        pub.blocking_send(to_send)
        time.sleep(1/velocity)


def send_circular_trajectory(radius, center_config, velocity):
    hexapod = Hexapod(center_config.body_position.copy(), center_config.body_rotation.copy(), morphology_config)
    to_send = center_config.copy()

    for config in tgen.body_circular_trajectory(radius, center_config):
        to_send = hexapod.configuration_to_new_body_pose(
                                            center_config.copy(),
                                            config.body_position,
                                            config.body_rotation)
        to_send.body_position = config.body_position
        to_send.body_rotation = config.body_rotation

        pub.blocking_send(to_send)
        time.sleep(1/velocity)


pub.blocking_send(central_config)
time.sleep(1)

speed = 50
send_linear_trajectory(central_config, north_config, speed)
send_linear_trajectory(north_config, south_config, speed)
send_linear_trajectory(south_config, central_config, speed)
send_linear_trajectory(central_config, west_config, speed)
send_linear_trajectory(west_config, east_config, speed)

send_circular_trajectory(distance, central_config, speed)

send_linear_trajectory(east_config, central_config, speed)

distance_angle = np.radians(10)


def rotation(angle, direction):
    config = central_config.copy()
    hexapod = Hexapod(config.body_position.copy(), config.body_rotation.copy(), morphology_config)
    config.body_rotation = np.array((0,) + direction)
    return config, hexapod.configuration_to_new_body_pose(
                                            config.copy(),
                                            config.body_position.copy(),
                                            np.array((angle,) + direction)
                                            )

config, roll_p_config = rotation(distance_angle, (1, 0, 0))
config, roll_n_config = rotation(-distance_angle, (1, 0, 0))
send_linear_trajectory(config, roll_p_config, speed)
send_linear_trajectory(roll_p_config, roll_n_config, speed)
send_linear_trajectory(roll_n_config, config, speed)

config, pitch_p_config = rotation(distance_angle, (0, 1, 0))
config, pitch_n_config = rotation(-distance_angle, (0, 1, 0))
send_linear_trajectory(config, pitch_p_config, speed)
send_linear_trajectory(pitch_p_config, pitch_n_config, speed)
send_linear_trajectory(pitch_n_config, config, speed)

config, yaw_p_config = rotation(distance_angle, (0, 0, 1))
config, yaw_n_config = rotation(-distance_angle, (0, 0, 1))
send_linear_trajectory(config, yaw_p_config, speed)
send_linear_trajectory(yaw_p_config, yaw_n_config, speed)
send_linear_trajectory(yaw_n_config, config, speed)
