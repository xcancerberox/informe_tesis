body_config = {
        'body_length': 100,
        'body_width': 100,
        'body_height': 10
        }

legs_lengths = (23, 55, 70)

offset = 15
offset_angle = 26.5

legs_config = [
        {
            'position': (
                body_config['body_width']/2 - offset,
                body_config['body_length']/2,
                0,
                1),
            'rotation': (offset_angle, 0, 0, 1)
            },
        {
            'position': (
                body_config['body_width']/2,
                0,
                0,
                1),
            'rotation': (0, 0, 0, 1)
            },
        {
            'position': (
                body_config['body_width']/2 - offset,
                -body_config['body_length']/2,
                0,
                1),
            'rotation': (-offset_angle, 0, 0, 1)
            },
        {
            'position': (
                -body_config['body_width']/2 + offset,
                body_config['body_length']/2,
                0,
                1),
            'rotation': (180-offset_angle, 0, 0, 1)
            },
        {
            'position': (
                -body_config['body_width']/2,
                0,
                0,
                1),
            'rotation': (180, 0, 0, 1)
            },
        {
            'position': (
                -body_config['body_width']/2 + offset,
                -body_config['body_length']/2,
                0,
                1),
            'rotation': (180+offset_angle, 0, 0, 1)
            },
        ]

legs_initial_angles = [
        [0, 45, 45],
        [0, 45, 45],
        [0, 45, 45],
        [0, 45, 45],
        [0, 45, 45],
        [0, 45, 45],
        ]

stride = 30
angle_stride = 10
velocity = 30
