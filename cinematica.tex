\chapter{Cinemática}

El estudio cinemático de robots paralelos suele ser una tarea compleja según
los grados de libertad del mismo. Para entender este tipo de robots se hace
primero el estudio completo sobre un robot con menos grados de libertad y una
vez solucionados los distintos problemas que surjen en el estudio se aborda
el robot más complejo.

\section{Estudio cinemático-estático de un robot paralelo sencillo}

El robot que se estudiará en esta sección es el robot plano caminante de tres 
grados de libertad (figura \ref{fig:esquemarobotplano3dof}). El estudio se hace
de forma incremental, inicialmente con un robot de una sola pata y se
agregan las otras dos progresivamente. De esta forma se introduce el primer
problema que es la redundancia de actuación.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/eps/esquemarobotplano3dof.pdf}
		\caption{Esquema general del robot plano de 3 grados de libertad.}
		\label{fig:esquemarobotplano3dof}
	\end{center}
\end{figure}

\subsection{Una pata con tres ejes con control activo} \label{una pata}

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemarobotplanounlimbs}
  \begin{lrbox}{\esquemarobotplanounlimbs}
		\input{imag/tex/esquema_robot_plano_1limb.tex} %croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\esquemarobotplanounlimbs}}
		\caption{Robot plano de cadena abierta.}
		\label{fig:esquema_robot_plano_1limbs}
\end{figure}

La única pata (Fig. \ref{fig:esquema_robot_plano_1limbs}) de la máquina
caminante tiene tres ejes de rotación $A_1$, $B_1$, y $C_1$ normales a la tierra y
actuados, que comandan los ángulos $\psi_1, \varphi_1$ y $\theta_1$.  En
principio, el cuerpo de la plataforma y pata no tienen rozamiento con el plano
sobre el que se mueven. Sin embargo, para poder moverse (trasladar y rotar) la
máquina deberá operar con la siguiente secuencia:

\begin{itemize}
  \item Al iniciar una etapa de transferencia de la plataforma el extremo del
        eje A de la pata se ``clava'' en la tierra de tal manera de poder
        absorber la fuerza en el plano (que permite trasladar la plataforma) y
        el momento generado por el actuador en A (que permite controlar el
        ángulo $\psi_1$).
  \item Terminada una etapa de transferencia el extremo de la pata se
        ``desclava'' para moverse a una nueva posición de soporte. Ahora es la
        plataforma la que se fija a la tierra.
  \item Se repite la secuencia anterior.
\end{itemize}

En estas condiciones la máquina caminante se comporta en cada fase como un
manipulador serie de tres gdl. La matrices Jacobianas que relacionan la velocidad
cartesiana $\dot{\overline{x}}=[\begin{array}{cc} {\overline{v_d}}^t &
\dot{\phi}\end{array}]^t$ de la plataforma con las velocidades
$\dot{\overline{q_1}}= [\begin{array}{ccc} \dot{\psi_1} & \dot{\varphi_1} &
\dot{\theta_1}\end{array}]^t$ de los ejes de la pata puede obtenerse derivando
la relación 
\begin{equation}
	\overline{A_{1}D} = \overline{A_{1}B_1} + \overline{B_{1}C_1} + \overline{C_{1}D}
	                                               \label{eq:pos_loop_1}
\end{equation}

\noindent para obtener

\begin{equation}
	\left\{ \begin{array}{l} 
		\overline{v_d} + \phi (\overline{e_1}\times\overline{k}) = \dot{\psi} [\overline{k}\times(\overline{a_1}+\overline{b_1})] + \dot{\varphi} (\overline{k}\times\overline{e_1})\\
		\dot{\phi} = \dot{\psi_1} + \dot{\varphi_1} + \dot{\theta_1}
	\end{array} \right.\\
\end{equation}
\begin{eqnarray*}
	&&\left[
		\begin{array}{cc} 
			I_{2\times2} & (\overline{e_1}\times\overline{k}) \\
			0_{1\times2} & 1
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\overline{v_d}\\
			\dot{\phi}
		\end{array}
	\right] =\\ 
	&&\left[
		\begin{array}{ccc} 
			(\overline{k}\times(\overline{a_1} + \overline{b_1})) & (\overline{k}\times\overline{b_1}) & 0_{2\times1}\\
			1 & 1 & 1
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\dot{\psi_1}\\
			\dot{\varphi_1}\\
			\dot{\theta_1}
		\end{array}
	\right]\\ 
 &&J_{x_1}\dot{\overline{x}} = J_{q_1} \dot{\overline{q_1}}  
\end{eqnarray*}

\noindent o

\begin{eqnarray*}
  \dot{\overline{q_1}} &=& J_{q_1}^{-1} J_x \dot{\overline{x}} \\
                 &=&  J_1 \dot{\overline{x}}
\end{eqnarray*}

Con el resultado anterior y utilizando el principio de los trabajos virtuales
(\ref{sec:trabajosvirtuales}) puede obtenerse también la ecuación de equilibrio
estático. Si $\overline{\tau_1} = [\begin{array}{ccc}\tau_{\psi_1} &
\tau_{\varphi_1} & \tau_{\theta_1}\end{array}]^t$ es el vector de los torques
generados por los actuadores en los ejes, y $\overline{F} = [\begin{array}{cc} 
\overline{f} & M_{z_d} \end{array}]$ es el vector de fuerza y momento exterior
respecto del punto D que puede realizar la plataforma accionada por la pata,
entonces

\begin{equation*}
  {\overline{F}}^t \dot{\overline{x}} = {\overline{\tau_1}}^t \dot{\overline{q_1}}
   =  {\overline{\tau_1}}^t J_1 \dot{\overline{x}}
\end{equation*}

\noindent y por lo tanto

\begin{equation*}
  {\overline{F}}= {J_1}^t {\overline{\tau_1}}
\end{equation*}

Como $J_x$, $J_{q_1}$ y por lo tanto $J_1 = J_{q_1}^{-1} J_x$ son matrices de
3x3 en general no singulares, existe la transformación directa e inversa entre
los espacios cartesiano y \emph{joint} tanto para la cinemática como para la estática.

\subsection{Se adiciona una segunda pata a la máquina caminante}

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemarobotplanodoslimbs}
  \begin{lrbox}{\esquemarobotplanodoslimbs}
		\input{imag/tex/esquema_robot_plano_2limb.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\esquemarobotplanodoslimbs}}
		\caption{Robot plano de cadena cerrada con 2 patas.}
		\label{fig:esquema_robot_plano_2limbs}
\end{figure}

Con la adición de la nueva pata (ver figura \ref{fig:esquema_robot_plano_2limbs}), se agrega ahora la relación

\begin{equation}
	\overline{A_{2}D} = \overline{A_{2}B_2} + \overline{B_{2}C_2} + \overline{C_{2}D}   
	                                               \label{eq:pos_loop_2}
\end{equation}

\noindent que junto con la Ec. \ref{eq:pos_loop_1} permiten obtener el sistema de ecuaciones:

\begin{eqnarray}
	\overline{v_d} + \dot{\phi} (\overline{e_1}\times\overline{k}) = \dot{\psi_1}	[\overline{k}\times(\overline{a_1}+\overline{b_1})] + \dot{\varphi_1} (\overline{k}\times\overline{e_1})\label{eq:vel_loop_1}\\
	\dot{\phi} = \dot{\psi_1} + \dot{\varphi_1} + \dot{\theta_1}\label{eq:relacion_ang_1}\\
	\overline{v_d} + \dot{\phi} (\overline{e_2}\times\overline{k}) = \dot{\psi_2}	[\overline{k}\times(\overline{a_2}+\overline{b_2})] + \dot{\varphi_2} (\overline{k}\times\overline{e_2})\label{eq:vel_loop_2}\\
	\dot{\phi} = \dot{\psi_2} + \dot{\varphi_2} + \dot{\theta_2}\label{eq:relacion_ang_2}
\end{eqnarray}

\noindent En forma matricial:

\begin{eqnarray*}
	\left[
		\begin{array}{cc} 
			H_{1}\\
			\hline
			H_{2}
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\overline{v_d}\\
			\dot{\phi}
		\end{array}
	\right] &=&
	\left[
		\begin{array}{c|c}
			C_{1} & \overline{0_{3\times3}}\\
			\hline
			\overline{0_{3\times3}} & C_{2}
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\dot{\psi_1}\\
			\dot{\varphi_1}\\
			\dot{\theta_1}\\
			\dot{\psi_2}\\
			\dot{\varphi_2}\\
			\dot{\theta_2}
		\end{array}
	\right] 
\end{eqnarray*}
\noindent con
\begin{eqnarray*}
	H_{i} &=& \left[
		\begin{array}{ccc|ccc}
			I_{2\times2} & (\overline{e_i}\times\overline{k}) \\
			0_{1\times2} & 1 
		\end{array}
	\right]\\
	C_{i} &=& \left[
		\begin{array}{ccc|ccc}
			(\overline{k}\times(\overline{a_1} + \overline{b_1})) & (\overline{k}\times\overline{b_1})\\
			1 & 1 & 1
		\end{array}
	\right]
\end{eqnarray*}

\noindent que nuevamente se puede escribir como 

\begin{equation}
 J_{x_2}\dot{\overline{x}} = J_{q_2} \dot{\overline{q_2}}
\end{equation}

Pero ahora $J_{x_2}$ es una matriz de $6\times3$, $J_{q_2}$ de $6\times6$ y en
consecuencia $J_2 = J_{q_2}^{-1} J_{x_2}$ de $6\times3$.

Una velocidad cartesiana $\dot{\overline{x}}$ arbitraria deseada, siempre tiene
un vector $\dot{\overline{q_2}}$ de velocidades \emph{joint} que la genera, pero un
vector $\dot{\overline{q_2}}$ arbitrario no puede aplicarse al mecanismo. Por
el mismo motivo, un vector arbitrario ${\overline{\tau_2}}$ aplicado por los
actuadores en los ejes, generará una única fuerza y momento $\overline{F}$ en
la plataforma. Sin embargo, para el mismo vector $\overline{F}$ aplicado sobre
la plataforma puede encontrarse más de un vector ${\overline{\tau_2}}$ que lo
genera. Estos resultados son consecuencia de que la máquina caminante de
nuestro ejemplo ahora tiene más actuadores que la dimensión del espacio de
movimiento de la plataforma.

\subsubsection{Manejo de la redundancia}

Se elegirá un conjunto de $3$ actuadores entre los $6$ existentes, para
controlar el movimiento de la plataforma durante la fase de transferencia. Los
$3$ ejes que se suponen sin actuadores o ejes pasivos, deberán acomodarse al
movimiento resultante de las patas por acción de los otros $3$ ejes actuados.
La relación matemática entre los espacios cartesiano y \emph{joint} deberá volver a
expresarse mediante una matriz Jacobiana cuadrada de $3\times3$.

En nuestro caso se liberarán del control de los actuadores los ejes $A_1$,
$A_2$ y $B_2$. Ahora, durante la etapa de transferencia de la plataforma, los
extremos de las patas sólo necesitarán evitar que los puntos de apoyo $A_1$ y
$A_2$ deslicen sobre la tierra, no tendrán que absorber momento.

Para lograr que las matrices $J_{x_2}$ y $J_{q_2}$ sean de $3\times3$ deben
eliminarse las tres filas correspondientes a los ángulos $\psi_1, \psi_2$ y
$\varphi_2$. Reagrupando las Ecs. (\ref{eq:vel_loop_1}) -
(\ref{eq:relacion_ang_2}) se eliminan $\psi_1$ y $\psi_2$.

\begin{eqnarray}
	\lefteqn{\overline{v_d} + \dot{\phi}[\overline{k}\times(\overline{a_1}+\overline{b_1}+\overline{e_1})]=} \notag \\
	& & \dot{\varphi_1} (\overline{a_1}\times\overline{k}) +
	\dot{\theta_i}[(\overline{a_1}+\overline{b_1})\times\overline{k}]\label{eq:vel_loop_simplificado_1} \\
\lefteqn{\overline{v_d} + \dot{\phi}[\overline{k}\times(\overline{a_2}+\overline{b_2}+\overline{e_2})] =} \notag \\
	& & \dot{\varphi_2} (\overline{a_2}\times\overline{k}) +
	\dot{\theta_2}[(\overline{a_2}+\overline{b_2})\times\overline{k}]\label{eq:vel_loop_simplificado_2} 
\end{eqnarray}

Luego, para eliminar $\varphi_2$ se multiplican ambos miembros de la Ec.
(\ref{eq:vel_loop_simplificado_2}) por $\overline{a_2}$ \cite{RobotAnalysisTsai}.

\begin{equation}
	\overline{a_2}\cdot\overline{v_d} + \dot{\phi}
	\overline{k}\cdot[\overline{a_2}\times(\overline{b_2}+\overline{e_2})]=
	\dot{\theta_2}\overline{k}\cdot[\overline{a_2}\times\overline{b_2}]
\end{equation}

Con la anterior y la Ec. (\ref{eq:vel_loop_simplificado_1}) se construyen las
matrices jacobianas cuadradas

\begin{multline*}
	\left[
		\begin{array}{cc} 
		I_{2\times2} & (\overline{a_1}+\overline{b_1}+\overline{e_1})\times\overline{k}\\
		\overline{a_2}^t & \overline{k}\cdot(\overline{a_2}\times(\overline{b_2}+\overline{e_2}))
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\overline{v_d}\\
			\dot{\phi}
		\end{array}
	\right] =\\
	\left[
		\begin{array}{ccc}
			(\overline{a_1}\times\overline{k}) & ((\overline{a_1}+\overline{b_1})\times\overline{k}) & \overline{0_{2\times1}}\\
			0 & 0 & \overline{k}\cdot(\overline{a_2}\times\overline{b_2})
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\dot{\varphi_1}\\
			\dot{\theta_1}\\
			\dot{\theta_2}
		\end{array}
	\right] 
\end{multline*}

\subsection{Se adiciona la tercer pata a la máquina caminante}

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemarobotplanotreslimbs}
  \begin{lrbox}{\esquemarobotplanotreslimbs}
		\input{imag/tex/esquema_robot_plano_3limbs.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\esquemarobotplanotreslimbs}}
		\caption{Robot plano de cadena cerrada con 3 patas.}
		\label{fig:esquema_robot_plano_3limbs}
\end{figure}

Al adicionar la tercer pata (figura \ref{fig:esquema_robot_plano_3limbs}), se
agrega entonces la relación

\begin{equation}
	\overline{A_{3}D} = \overline{A_{3}B_3} + \overline{B_{3}C_3} + \overline{C_{3}D}   
	                                               \label{eq:pos_loop_3}
\end{equation}

\noindent que junto con las Ecs. (\ref{eq:pos_loop_1}) y (\ref{eq:pos_loop_2})
generaría ahora una matriz $J_{x_3}$ de $9\times3$ y una $J_{q_3}$ de
$9\times9$. Sin embargo, utilizando el método antes descripto para manejar las
redundancias (se elige $C_1$, $C_2$ y $C_3$ como ejes actuados) se obtienen los
siguientes jacobianos cuadrados

\begin{multline*}
	\left[
		\begin{array}{cc} 
		\overline{a_1}^t & \overline{k}\cdot(\overline{a_1}\times(\overline{b_1}+\overline{e_1}))\\
		\overline{a_2}^t & \overline{k}\cdot(\overline{a_2}\times(\overline{b_2}+\overline{e_2}))\\
		\overline{a_3}^t & \overline{k}\cdot(\overline{a_3}\times(\overline{b_3}+\overline{e_3}))
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\overline{v_d}\\
			\dot{\phi}
		\end{array}
	\right] =\\
	\left[
		\begin{array}{ccc}
			\overline{k}\cdot(\overline{a_1}\times\overline{b_1}) & 0 & 0\\
			0 & \overline{k}\cdot(\overline{a_2}\times\overline{b_2}) & 0\\
			0 & 0 & \overline{k}\cdot(\overline{a_3}\times\overline{b_3})
		\end{array}
	\right]
	\left[
		\begin{array}{c} 
			\dot{\theta_1}\\
			\dot{\theta_2}\\
			\dot{\theta_3}
		\end{array}
	\right]
\end{multline*}

Con la máquina caminante construida de esta manera, durante la fase de
transferencia de la plataforma los ejes $A_i$ y $B_i$ ($i=1,2,3$) se comportan
como ejes pasivos (sus actuadores no desarrollan torque) acomodándose al
movimiento resultante controlado por los ejes activos.

\subsection{Cinemática inversa del robot}\label{sec:estudiocinematico}

Cuando se desea obtener los ángulos para cada actuador en base a la posición y
orientación deseadas en el cuerpo del robot se utiliza la cinemática inversa
del robot, donde se tiene la posición $(x_A,y_A)$ y la orientación $\phi$ y se
calculan los ángulo $\theta_1,\theta_2,\theta_3$.

Por ser un robot sencillo se buscará una solución directa utilizando relaciones
trigonométricas. Se comienza planteando los puntos $B$ y $C$ respecto de las
variables conocidas.

\begin{eqnarray*}
	x_B &=& x_A + h \cos{\phi}\\
	y_B &=& y_A + h \sin{\phi}\\
	x_C &=& x_A + h \cos{\phi + \frac{\pi}{3}}\\
	y_C &=& y_A + h \sin{\phi + \frac{\pi}{3}}
\end{eqnarray*}

Además para cada uno de los 3 puntos que definen la plataforma móvil se puede
encontrar una relación entre el ángulo actuado del miembro correspondiente y
del punto de apoyo según la figura \ref{fig:esquema_robot_plano_limbs}.

\begin{figure}
	\begin{center}
		\includegraphics[width =0.6\textwidth]{imag/eps/esquema_robot_plano_limbs.pdf}
		\caption{Geometría del mecanismo.}
		\label{fig:esquema_robot_plano_limbs}
	\end{center}
\end{figure}

Tomando el origen de coordenadas en el punto P se obtienen las siguientes
ecuaciones.

\begin{eqnarray*}
	x_A &=& a_1 \cos{\theta_1} + b_1 \cos{\theta_1 + \psi_1}\\
	y_A &=& a_1 \sin{\theta_1} + b_1 \sin{\theta_1 + \psi_1}\\
	x_B &=& x_Q + a_2 \cos{\theta_2} + b_2 \cos{\theta_2 + \psi_2}\\
	y_B &=& y_Q + a_2 \sin{\theta_2} + b_2 \sin{\theta_2 + \psi_2}\\
	x_C &=& x_R + a_3 \cos{\theta_3} + b_3 \cos{\theta_3 + \psi_3}\\
	y_C &=& y_R + a_3 \sin{\theta_3} + b_3 \sin{\theta_3 + \psi_3}
\end{eqnarray*}

Uniendo los dos sistemas de ecuaciones.

\begin{eqnarray*}
    x_A &=& a_1 \cos{\theta_1} + b_1 \cos{\theta_1 + \psi_1} \notag\\
    &\Rightarrow& x_A - a_1 \cos{\theta_1} = b_1 \cos{\theta_1 + \psi_1}\\
    y_A &=& a_1 \sin{\theta_1} + b_1 \sin{\theta_1 + \psi_1} \notag\\
    &\Rightarrow& y_A - a_1 \sin{\theta_1} = b_1 \sin{\theta_1 + \psi_1}
\end{eqnarray*}

Para quitar el ángulo $\psi$ del sistema se elevan las dos ecuaciones al
cuadrado y se suman

\begin{equation}
	x_A^2 + y_A^2 - 2 x_A a_1 \cos{\theta_1} - 2 y_A a_1 \sin{\theta_1} + a_1^2 - b_1^2 = 0
	\label{eq:ecuacion_limb1}
\end{equation}

\noindent Haciendo lo mismo para los otros dos miembros se obtienen las ecuaciones
\ref{eq:ecuacion_limb2} y \ref{eq:ecuacion_limb3}.

\begin{eqnarray*}
    x_A + h \cos{\phi} &=& x_Q + a_2 \cos{\theta_2} + b_2 \cos{\theta_2 + \psi_2} \notag\\
    &\Rightarrow& x_A + h \cos{\phi} - x_Q - a_2 \cos{\theta_2} = b_2 \cos{\theta_2 + \psi_2}\\
    y_A + h \sin{\phi} &=& y_Q + a_2 \sin{\theta_2} + b_2 \sin{\theta_2 + \psi_2} \notag\\
    &\Rightarrow& y_A + h \sin{\phi} - y_Q - a_2 \sin{\theta_2} = b_2 \sin{\theta_2 + \psi_2}
\end{eqnarray*}
\begin{eqnarray}
	x_A^2 + y_A^2 - 2 x_A x_Q - 2 y_A y_Q + x_Q^2 + y_Q^2 + h^2 + a_2^2 - b_2^2 + 2 x_A h \cos{\phi} \notag\\
	+ 2 y_A h \sin{\phi} - 2 x_A a_2 \cos{\theta_2} - 2 y_A a_2 \sin{\theta_2} - 2 x_Q h \cos{\phi}  \notag\\
	- 2 y_Q h \sin{\phi} + 2 x_Q a_2	\cos{\theta_2} + 2 y_Q a_2 \sin{\theta_2} = 0
	\label{eq:ecuacion_limb2}
\end{eqnarray}
\begin{eqnarray}
    x_A + h \cos{\phi + \frac{\pi}{3}} &=& x_R + a_3 \cos{\theta_3} + b_3 \cos{\theta_3 + \psi_3} \notag\\
    &\Rightarrow& x_A + h \cos{\phi + \frac{\pi}{3}} - x_R - a_3 \cos{\theta_3} = b_3 \cos{\theta_3 + \psi_3}\\
    y_A + h \sin{\phi + \frac{\pi}{3}} &=& y_R + a_3 \sin{\theta_3} + b_3 \sin{\theta_3 + \psi_3} \notag\\
    &\Rightarrow& y_A + h \sin{\phi + \frac{\pi}{3}} - y_R - a_3 \sin{\theta_3} = b_3 \sin{\theta_3 + \psi_3}
\end{eqnarray}
\begin{eqnarray}
	x_A^2 + y_A^2 - 2 x_A x_R - 2 y_A y_R + x_R^2 + y_R^2 + h^2 + a_3^2 - b_3^2 + 2 x_A h \cos{\phi + \frac{\pi}{3}} \notag\\
	+ 2 y_A h \sin{\phi + \frac{\pi}{3}} - 2 x_A a_3 \cos{\theta_3} - 2 y_A a_3 \sin{\theta_3} - 2 x_R h \cos{\phi + \frac{\pi}{3}}  \notag\\
	- 2 y_R h \sin{\phi + \frac{\pi}{3}} + 2 x_R a_3 \cos{\theta_3} + 2 y_R a_3 \sin{\theta_3} = 0
	\label{eq:ecuacion_limb3}
\end{eqnarray}

Con las ecuaciones \ref{eq:ecuacion_limb1}, \ref{eq:ecuacion_limb2} y
\ref{eq:ecuacion_limb3} se arma un sistema de 3 ecuaciones en senos y cosenos
de los ángulos buscados.

\begin{eqnarray*}
	e_{11} \sin{\theta_1} + e_{12} \cos{\theta_1} + e_{13} &= 0 \label{eq:ecuacion_plano_theta1}\\
	e_{21} \sin{\theta_2} + e_{22} \cos{\theta_2} + e_{23} &= 0 \label{eq:ecuacion_plano_theta2}\\
	e_{31} \sin{\theta_3} + e_{32} \cos{\theta_3} + e_{33} &= 0 \label{eq:ecuacion_plano_theta3}
\end{eqnarray*}

\noindent donde,

\begin{eqnarray*}
	e_{11} &=& - 2 y_A a_1\\
	e_{12} &=& - 2 x_A a_1\\
	e_{13} &=& x_A^2 + y_A^2 + a_1^2 - b_1^2\\
	e_{21} &=& 2 y_Q a_2 - 2 a_2 h \sin{\phi} - 2 y_A a_2\\
	e_{22} &=& 2 x_Q a_2 - 2 a_2 h \cos{\phi} - 2 x_A a_2\\
	e_{23} &=& x_A^2 + y_A^2 + a_2^2 - b_2^2 - 2 x_A x_q - 2 y_A y_Q + x_Q^2\\
           &+& y_Q^2 + h^2 + 2 x_A h \cos{\phi} + 2 y_A h \sin{\phi} - 2 x_Q h \cos{\phi} - 2 y_Q h \sin{\phi}\\
	e_{31} &=& 2 y_R a_3 - 2 a_3 h \sin{\phi + \frac{\pi}{3}} - 2 y_A a_3\\
	e_{32} &=& 2 x_R a_3 - 2 a_3 h \cos{\phi + \frac{\pi}{3}} - 2 x_A a_3\\
	e_{33} &=& x_A^2 + y_A^2 + a_3^2 - b_3^2 - 2 x_A x_R - 2 y_A y_R + x_R^2\\
           &+& y_R^2 + h^2 + 2 x_A h \cos{\phi + \frac{\pi}{3}} + 2 y_A h \sin{\phi + \frac{\pi}{3}}\\
           &-& 2 x_R h \cos{\phi + \frac{\pi}{3}} - 2 y_R h \sin{\phi + \frac{\pi}{3}}
\end{eqnarray*}

Utilizando las siguientes identidades trigonométricas

\begin{eqnarray*}
	\sin{\theta} &=& \frac{2 \tan{\frac{\theta}{2}}}{1+\tan{\frac{\theta}{2}}^2} = \frac{2 t}{1+t^2}\\
	\cos{\theta} &=& \frac{1-\tan{\frac{\theta}{2}}^2}{1+\tan{\frac{\theta}{2}}^2} = \frac{1-t^2}{1+t^2}
\end{eqnarray*}

\noindent se pueden llevar las tres ecuaciones a tres polinomios de grado dos en las
tangentes de los ángulos, pudiendo obtener dos soluciones para cada tangente.

\begin{align*}
	t_1^2 (e_{13} - e_{12}) &+ t_1 (2 e_{11}) + (e_{12} + e_{13}) = 0\\
    &\Rightarrow t_{11} = \frac{-e_{11} + \sqrt{ e_{11}^2 + e_{12}^2 - e_{13}^2}}{e_{13} - e_{12}}\\
    &\Rightarrow t_{12} = \frac{-e_{11} - \sqrt{ e_{11}^2 + e_{12}^2 - e_{13}^2}}{e_{13} - e_{12}}\\
	t_2^2 (e_{23} - e_{22}) &+ t_2 (2 e_{21}) + (e_{22} + e_{23}) = 0\\
    &\Rightarrow t_{21} = \frac{-e_{21} + \sqrt{ e_{21}^2 + e_{22}^2 - e_{23}^2}}{e_{23} - e_{22}}\\
    &\Rightarrow t_{22} = \frac{-e_{21} - \sqrt{ e_{21}^2 + e_{22}^2 - e_{23}^2}}{e_{23} - e_{22}}\\
	t_3^2 (e_{33} - e_{32}) &+ t_3 (2 e_{31}) + (e_{32} + e_{33}) = 0\\
    &\Rightarrow t_{31} = \frac{-e_{31} + \sqrt{ e_{31}^2 + e_{32}^2 - e_{33}^2}}{e_{33} - e_{32}}\\
    &\Rightarrow t_{32} = \frac{-e_{31} - \sqrt{ e_{31}^2 + e_{32}^2 - e_{33}^2}}{e_{33} - e_{32}}
\end{align*}

Así se obtienen dos soluciones para cada ángulo. Para evitar problemas con los
cuadrantes se utiliza atan2 reconstruyendo el seno y el coseno de cada ángulo.

\begin{align*}
	\theta_{11} &= atan2(2 t_{11}, 1-t_{11}^2)\\
	\theta_{12} &= atan2(2 t_{12}, 1-t_{12}^2)\\
	\theta_{21} &= atan2(2 t_{21}, 1-t_{21}^2)\\
	\theta_{22} &= atan2(2 t_{22}, 1-t_{22}^2)\\
	\theta_{31} &= atan2(2 t_{31}, 1-t_{31}^2)\\
	\theta_{32} &= atan2(2 t_{32}, 1-t_{32}^2)
\end{align*}

\subsubsection{Cinemática directa del robot}

En este problema se tienen como entrada los ángulos de cada uno de los ejes
actuados $\theta_1$, $\theta_2$ y $\theta_3$ y se quieren resolver la posición
del cuerpo $(x_A,y_A)$ y su orientación $\phi$.

Volviendo a las ecuaciones de los tres miembros (\ref{eq:ecuacion_limb1},
\ref{eq:ecuacion_limb2} y \ref{eq:ecuacion_limb3}) se arma un sistema de 3
ecuaciones con $x_A$ e $y_A$.

\begin{eqnarray}
	x_A^2 + y_A^2 + e_{11} x_A + e_{12} y_A + e_{13} &=& 0 \label{eq:ecuacion_nolin_pos1}\\
	x_A^2 + y_A^2 + e_{21} x_A + e_{22} y_A + e_{23} &=& 0 \label{eq:ecuacion_nolin_pos2}\\
	x_A^2 + y_A^2 + e_{31} x_A + e_{32} y_A + e_{33} &=& 0 \label{eq:ecuacion_nolin_pos3}
\end{eqnarray}

\noindent donde,

\begin{eqnarray*}
	e_{11} &=& - 2 a_1 \cos{\theta_1}\\
	e_{12} &=& - 2 a_1 \sin{\theta_1}\\
	e_{13} &=& a_1^2 - b_1^2\\
	e_{21} &=& - 2 x_Q + 2 h \cos{\phi} - 2 a_2 \cos{\theta_2}\\
	e_{22} &=& - 2 y_Q + 2 h \sin{\phi} - 2 a_2 \sin{\theta_2}\\
	e_{23} &=& x_Q^2 + y_Q^2 + h^2 + a_2^2 - b_2^2 -2 a_2 h \cos{\theta_2} \cos{\phi} - 2 a_2 h \sin{\theta_2} \sin{\phi}\\
           &-& 2 x_Q h \cos{\phi} - 2 y_Q h \sin{\phi} + 2 x_Q a_2 \cos{\theta_2} + 2 y_Q a_2 \sin{\theta_2}\\
	e_{31} &=& - 2 x_R + 2 h \cos{\phi + \frac{\pi}{3}} - 2 a_3 \cos{\theta_3}\\
	e_{32} &=& - 2 y_R + 2 h \sin{\phi + \frac{\pi}{3}} - 2 a_3 \sin{\theta_3}\\
	e_{33} &=& x_R^2 + y_R^2 + h^2 + a_3^2 - b_3^2 - 2 a_3 h \cos{\theta_3} \cos{\phi + \frac{pi}{3}} - 2 a_3 h \sin{\theta_3} \sin{\phi + \frac{pi}{3}}\\
	       &-& 2 x_R h \cos{\phi + \frac{\pi}{3}} - 2 y_R h \sin{\phi + \frac{\pi}{3}} + 2 x_R a_3 \cos{\theta_3} + 2 y_R a_3 \sin{\theta_3}
\end{eqnarray*}

En el apéndice \ref{sec:solucioncinematicadirecta} utilizando varios cambios de
variables se obtiene el polinomio de octavo grado
\ref{eq:polinomiocinematicadirecta} que permite obtener las ocho soluciones
para $t = \tan{\frac{\phi}{2}}$ que son ocho soluciones de la orientación $\phi
= 2 \arctan{t}$. Para cada una de estas soluciones y utilizando las ecuaciones
\ref{eq:ecuacion_delta1}, \ref{eq:ecuacion_delta2}, \ref{eq:ecuacion_delta}, y
sabiendo que $x_{A} = \frac{\delta_1}{\delta}$ y $y_{A} =
\frac{\delta_2}{\delta}$ se calcula el par $(x_A, y_A)$ para cada valor de
orientación.

Con las soluciones presentadas en las dos secciones anteriores: las
correspondientes al problema inverso, que sabiendo la posición y la orientación
del cuerpo permite saber cuales son los ángulos que se deben poner en los
ejes actuados; y la correspondiente al problema directo, que dados los ángulos
de los ejes actuados se puede saber en que posición y orientación se encuentra
el cuerpo, se tiene resuelta la cinemática completa del robot.

\section{Estudio cinemático-estático de un robot paralelo caminante complejo}

El paso siguiente para el movimiento del robot complejo (figura
\ref{fig:caminantecomplejo}) es el calculo cinemático.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/caminantecomplejo.pdf}
    \caption{Esquema general del robot caminante de 6 gdl}
		\label{fig:caminantecomplejo}
	\end{center}
\end{figure}

En la sección anterior se mostraron los distintos problemas que surgen al
calcular la cinemática de un robot paralelo. Para hacer el robot complejo se
simplificará el problema dividiéndolo en dos partes que fueron explicadas en
los capítulos \ref{cap:gaits} y \ref{cap:trayectoria}. Primero se
estudiará la cinemática de las patas en su movimiento de translación tomándolas
como cadenas cinemáticas abiertas. Luego se plantearán los cálculos necesarios
para resolver los ángulos para situar la plataforma en una posición
y una orientación deseada.

\subsection{Cinemática de las patas en translación}

Cuando una pata se encuentra levantada del piso se comporta como un robot serie
de 3 grados de libertad (figura \ref{fig:esquemapata3dof}).

\begin{figure}[ht]
  \centering
  \newsavebox{\esquemapatatresdofbis}
  \begin{lrbox}{\esquemapatatresdofbis}
		\input{imag/tex/esquemapata3dof.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\esquemapatatresdofbis}}
	    \caption{Esquema de la pata de 3 grados de libertad. Robot serie con 3 articulaciones de rotación.}
		\label{fig:esquemapata3dof}
\end{figure}

El problema que se quiere resolver es el que permite obtener los ángulos a
partir de la posición de la punta de la pata $\bar{p} = \left[
\begin{array}{ccc} p_x & p_y & p_z \end{array}\right]$ relativa a la unión
de la pata con el cuerpo. Como este mecanismo es un poco más complejo que
el del caso del robot anterior, se utilizará el método de Denavit-Hartenberg
que se explicará a continuación.

\subsubsection{Parámetros de Denavit-Hartenberg}\label{sec:parametrosdh}

Como se explica en el apéndice \ref{sec:asignaciontramas}, una vez definidas
las tramas se pueden medir los parámetros de Denavit-Hartenberg que definen por
completo la cadena cinemática.

\begin{itemize}
	\item $a_{i}$ = la distancia de $\hat{Z}_{i}$ a $\hat{Z}_{i-1}$ medida sobre $\hat{X}_{i}$;
	\item $\alpha_{i}$ = el ángulo de $\hat{Z}_{i}$ a $\hat{Z}_{i-1}$ medido sobre $\hat{X}_{i}$;
	\item $d_{i}$ = la distancia de $\hat{X}_{i-1}$ a $\hat{X}_{i}$ medida sobre $\hat{Z}_{i}$; y
	\item $\theta_{i}$ = el ángulo de $\hat{X}_{i-1}$ a $\hat{X}_{i}$ medido sobre $\hat{Z}_{i}$.
\end{itemize}

La convención descrita no representa la única manera de asignar tramas a los
vínculos.  En el momento de alinear el eje $\hat{Z}_{i}$ con el eje de
articulación $i$ se pueden elegir dos direcciones distintas para $\hat{Z}_{i}$.
Lo mismo cuando elegimos el sentido de $\hat{X}_{i}$ en la dirección de $a_{i}$
también tenemos dos opciones. En el caso en que los ejes $i$ e $i+1$ son
paralelos, el origen de la trama ${i}$ es arbitrario (se elige de forma de que
$d_{i}$ sea cero, pero es por conveniencia).

\begin{figure}[ht]
  \centering
  \newsavebox{\tramaspatatresdof}
  \begin{lrbox}{\tramaspatatresdof}
		\input{imag/tex/tramaspata3dof.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\tramaspatatresdof}}
	  \caption{Asignación de tramas a las articulaciones de la pata de 3 gdl.}
		\label{fig:tramaspata3dof}
\end{figure}

\subsubsection{Cálculo cinemático}

Utilizando lo antes explicado se obtienen los siguientes parámetros de
Denavit-Hartenberg para las tramas planteadas:

\begin{equation*}
  \begin{array}{c||c|c|c|c|}
      & a   & d & \alpha            & \theta   \\\hline
    1 & a_1 & 0 & -\frac{\pi}{2}    & \theta_1 \\\hline
    2 & a_2 & 0 & 0                 & \theta_2 \\\hline
    3 & a_3 & 0 & 0                 & \theta_3 \\\hline
  \end{array}
\end{equation*}

Se utiliza la solución para la transformación de vinculo del anexo
\ref{sec:transformacionvinculo} para obtener las matrices entre tramas:

\begin{eqnarray*}
    {^{0}T_{1}} &=& \left[\begin{array}{ccc|c}
                          \cos{\theta_1} & 0  & -\sin{\theta_1} & \cos{\theta_1} a_1\\
                          \sin{\theta_1} & 0  &  \cos{\theta_1} & \sin{\theta_1} a_1\\
                          0              & -1 &  0              & 0\\\hline
                          0              & 0  &  0              & 1
                    \end{array}\right]\\
    {^{1}T_{2}} &=& \left[\begin{array}{ccc|c}
                            \cos{\theta_2} & -\sin{\theta_2} & 0 & \cos{\theta_2} a_2\\
                            \sin{\theta_2} &  \cos{\theta_2} & 0 & \sin{\theta_2} a_2\\
                            0              & 0               & 1 & 0\\\hline
                            0              & 0               & 0 & 1
                    \end{array}\right]\\
    {^{2}T_{3}} &=& \left[\begin{array}{ccc|c}
                        \cos{\theta_3} & -\sin{\theta_3} & 0 & \cos{\theta_3} a_3\\
                        \sin{\theta_3} &  \cos{\theta_3} & 0 & \sin{\theta_3} a_3\\
                        0              & 0               & 1 & 0\\\hline
                        0              & 0               & 0 & 1
                    \end{array}\right]
\end{eqnarray*}

Se trabaja con las transformaciones según se explicó en la sección
\ref{sec:trasfhomogenea} para obtener la transformación ${^{0}T_{3}}$.
\tiny
\begin{eqnarray}
    {^{1}T_{3}} &=& {^{1}T_{2}} {^{2}T_{3}}\notag\\
                &=& \left[ \begin{array}{ccc|c}
                        (\cos{\theta_2}\cos{\theta_3}-\sin{\theta_2}\sin{\theta_3})&(-\cos{\theta_2}\sin{\theta_3}-\sin{\theta_2}\cos{\theta_3})&0&(\cos{\theta_2}\cos{\theta_3}a_3-\sin{\theta_2}\sin{\theta_3}a_3+\cos{\theta_2}a_2)\\
                        (\sin{\theta_2}\cos{\theta_3}+\cos{\theta_2}\sin{\theta_3})&(-\sin{\theta_2}\sin{\theta_3}+\cos{\theta_2}\cos{\theta_3})&0&(\sin{\theta_2}\cos{\theta_3}a_3-\cos{\theta_2}\sin{\theta_3}a_3+\sin{\theta_2}a_2)\\
                        0 & 0 & 1 & 0\\\hline
                        0 & 0 & 0 & 1
                    \end{array}\right]\notag\\
                &=& \left[ \begin{array}{ccc|c}
                        \cos{\theta_2+\theta_3}&-\sin{\theta_2+\theta_3}&0&\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2\\
                        \sin{\theta_2+\theta_3}& \cos{\theta_2+\theta_3}&0&\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2\\
                        0 & 0 & 1 & 0\\\hline
                        0 & 0 & 0 & 1
                    \end{array}\right]\notag\\
    {^{0}T_{3}} &=& {^{0}T_{1}} {^{1}T_{3}}\notag\\
                &=& \left[ \begin{array}{ccc|c}
                        \cos{\theta_1}\cos{\theta_2+\theta_3}&-\cos{\theta_1}\sin{\theta_2+\theta_3}&-\sin{\theta_1}&\cos{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2) + \cos{\theta_1} a_1\\
                        \sin{\theta_1}\cos{\theta_2+\theta_3}&-\sin{\theta_1}\sin{\theta_2+\theta_3}&\cos{\theta_1}&\sin{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2) + \sin{\theta_1} a_1\\
                        -\sin{\theta_2+\theta_3}& -\cos{\theta_2+\theta_3}&0&-(\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2)\\\hline
                        0 & 0 & 0 & 1
                    \end{array}\right]\notag\\
                &=& \left[ \begin{array}{ccc|c}
                        \cos{\theta_1}\cos{\theta_2+\theta_3}&-\cos{\theta_1}\sin{\theta_2+\theta_3}&-\sin{\theta_1}&\cos{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
                        \sin{\theta_1}\cos{\theta_2+\theta_3}&-\sin{\theta_1}\sin{\theta_2+\theta_3}&\cos{\theta_1} &\sin{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
                        -\sin{\theta_2+\theta_3}& -\cos{\theta_2+\theta_3}& 0 &-(\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2)\\\hline
                        0 & 0 & 0 & 1
                    \end{array}\right]\label{eq:tranfdirrobotserie}
\end{eqnarray}
\normalsize

Como se mostró en el apéndice \ref{sec:trasfhomogenea} la matriz de la
transformación esta formada por la sub matriz de rotación y el vector columna
que corresponde al vector de translación entre las tramas. También como se dijo
anteriormente la punta de la pata queremos que se encuentre en el punto
$\bar{p} = \left[ \begin{array}{ccc} p_x & p_y & p_z \end{array}\right]$, por
lo tanto:

\begin{equation*}
\bar{p} = \left[ \begin{array}{c} p_x \\ p_y \\ p_z \end{array}\right]^{T} = 
  \left[\begin{array}{c}
      \cos{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
      \sin{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
      -(\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2)
  \end{array}\right]
\end{equation*}

Se trabaja con esta igualdad para resolver los ángulos $\theta_1$, $\theta_2$ y
$\theta_3$. De la ecuación anterior se arma el sistema de ecuaciones:

\begin{equation}
  \left\{\begin{array}{cc}
      p_x & \cos{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
      p_y & \sin{\theta_1}(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)\\
      p_z & -(\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2)
  \end{array}\right.\label{eq:inversopata}
\end{equation}

Para el primero de los ángulos se utilizan las dos primeras ecuaciones
de \ref{eq:inversopata} despejando el coseno y el
seno:

\begin{eqnarray}
    \cos{\theta_1} &=& \frac{p_x}{(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)}\notag\\
    \sin{\theta_1} &=& \frac{p_y}{(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)}\notag\\
    \Rightarrow \theta_1 &=& \atandos{\sin{\theta_1}, \cos{\theta_1}}\notag\\
                         &=& \atandos{\frac{p_x}{(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)}, \frac{p_y}{(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)}}\notag\\
                         &=& \atandos{p_x, p_y}
\end{eqnarray}

Para resolver el ángulo $\theta_3$ se comienza elevando las dos primeras ecuaciones
de \ref{eq:inversopata} al cuadrado y sumándolas:

\begin{eqnarray}
    p^{2}_{x} + p^{2}_{y} &=& \cos{\theta_1}^{2} (\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)^{2} + \sin{\theta_1}^{2} (\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)^{2}\notag\\
                          &=& (\cos{\theta_1}^{2} + \sin{\theta_1}^{2}) (\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)^{2}\notag\\
                          &=& (\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2 + a_1)^{2}\notag\\
                          &\therefore& \sqrt{p^{2}_{x} + p^{2}_{y}} - a_1 = \cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2\label{eq:inversopataec4}
\end{eqnarray}

\noindent Se suma la ecuación \ref{eq:inversopataec4} y la tercer ecuación de 
\ref{eq:inversopata} elevadas al cuadrado:

\begin{eqnarray}
    \left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right)^{2} + p^{2}_{z} &=& \left(\cos{\theta_2+\theta_3} a_3 + \cos{\theta_2} a_2\right)^{2} + \left(\sin{\theta_2+\theta_3} a_3 + \sin{\theta_2} a_2\right)^{2}\notag\\
    &=& \cos{\theta_2+\theta_3}^2 a^{2}_{3} + \cos{\theta_2}^2 a^{2}_{2} + 2 \cos{\theta_2+\theta_3} \cos{\theta_2} a_2 a_3\notag\\
    &+& \sin{\theta_2+\theta_3}^{2} a^{2}_{3} + \sin{\theta_2}^{2} a^{2}_{2} + 2 \sin{\theta_2 + \theta_3} \sin{\theta_2} a_2 a_3\notag\\
    &=& \left(\cos{\theta_2+\theta_3}^2 + \sin{\theta_2+\theta_3}^{2}\right) a^{2}_{3} + \left(\cos{\theta_2}^2 + \sin{\theta_2}^{2}\right) a^{2}_{2}\notag\\
    &+& 2 \left(\cos{\theta_2+\theta_3} \cos{\theta_2} + \sin{\theta_2 + \theta_3} \sin{\theta_2}\right) a_2 a_3\notag\\
    &=& a^{2}_{3} + a^{2}_{2} + 2 \cos{\theta_2+\theta_3-\theta_2} a_2 a_3\notag\\
    &=& a^{2}_{3} + a^{2}_{2} + 2 \cos{\theta_3} a_2 a_3\notag\\
    &\therefore& \cos{\theta_3} = \frac{\left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right)^{2} + p^{2}_{z} - a^{2}_{3} - a^{2}_{2}}{2 a_2 a_3} = D \label{eq:cosenotheta3}
\end{eqnarray}

Usando la ecuación \ref{eq:cosenotheta3} y la identidad trigonométrica
$\sin{\theta_3} = \pm \sqrt{1 - \cos{\theta_3}^2}$ se calcula el ángulo mediante la tangente:

\begin{equation}
  \theta_3 = \atandos{\pm \sqrt{1 - D^2}, D}
\end{equation}

Para el ángulo $\theta_2$ se arma un sistema de ecuaciones en senos y cosenos
de este ángulo utilizando las tercer ecuación de \ref{eq:inversopata} y la
ecuación \ref{eq:inversopataec4}:

\begin{equation*}
  \left\{\begin{array}{cc}
      \cos{\theta_2} \left(\cos{\theta_3} a_3 + a_2\right) + \sin{\theta_2} \left(-\sin{\theta_3} a_3\right)&=\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\\
      \cos{\theta_2} \left(-\sin{\theta_3} a_3\right) + \sin{\theta_2} \left(\cos{\theta_3} a_3 + a_2\right)&= -p_{z}
  \end{array}\right.
\end{equation*}

\noindent Utilizando la regla de Cramer se despejan el seno y el coseno y
nuevamente se calcula el ángulo por medio de la tangente:
\tiny
\begin{eqnarray*}
    \cos{\theta_2} &=& \frac{\left(\cos{\theta_3} a_3 + a_2\right) \left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right) - \left(-\sin{\theta_3} a_3\right) p_z}{\left(-\sin{\theta_3} a_3\right)^{2} + \left(\cos{\theta_3} a_3 + a_2\right)^{2}}\\
    \sin{\theta_2} &=& \frac{-\left(\cos{\theta_3} a_3 + a_2\right) p_z - \left(-\sin{\theta_3} a_3\right)\left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right)}{\left(-\sin{\theta_3} a_3\right)^{2} + \left(\cos{\theta_3} a_3 + a_2\right)^{2}}\\
    \therefore \theta_2 &=& \atandos{\frac{\left(\cos{\theta_3} a_3 + a_2\right) \left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right) - \left(-\sin{\theta_3} a_3\right) p_z}{\left(-\sin{\theta_3} a_3\right)^{2} + \left(\cos{\theta_3} a_3 + a_2\right)^{2}}, \frac{-\left(\cos{\theta_3} a_3 + a_2\right) p_z - \left(-\sin{\theta_3} a_3\right)\left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right)}{\left(-\sin{\theta_3} a_3\right)^{2} + \left(\cos{\theta_3} a_3 + a_2\right)^{2}}}\\
                        &=& \atandos{\left(\cos{\theta_3} a_3 + a_2\right) \left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right) - \left(-\sin{\theta_3} a_3\right) p_z, -\left(\cos{\theta_3} a_3 + a_2\right) p_z - \left(-\sin{\theta_3} a_3\right) \left(\sqrt{p^{2}_{x} + p^{2}_{y}} - a_1\right)}
\end{eqnarray*}
\normalsize

Completando así la solución del problema inverso de cada pata cuando se
encuentra levantada del piso.

\subsection{Cinemática de cuerpos con $n$ patas en soporte}

Se supone el robot con seis patas apoyadas (figura
\ref{fig:caminantecomplejocinematica}) en una posición respecto a la terna
$\{O\}$ ${^{O}\bar{p}_{C}}$ y una orientación dada por una matriz de rotación
$R$. Tomando la pata 2 como ejemplo, se sabe que está en una posición fija
respecto del cuerpo $^{C}\bar{p}_{B_2}$ y que el pie estará en la posición
$^{B_2}\bar{p}_{2}$ respecto del inicio de la pata.

\begin{figure}[ht]
  \centering
  \newsavebox{\caminantecomplejocinematica}
  \begin{lrbox}{\caminantecomplejocinematica}
		\input{imag/tex/caminantecomplejocinematica.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\caminantecomplejocinematica}}
		\caption{Posición de la pata 2 respecto a distintas ternas.}
		\label{fig:caminantecomplejocinematica}
\end{figure}

En el capitulo \ref{cap:trayectoria} se plantearon los cálculos necesarios para
obtener las posiciones intermedias por las cuales deben pasar el cuerpo y las
patas a lo largo del \emph{gait}. De estos cálculos saldrán entonces la posición del
cuerpo deseada ${^{O}\bar{p}_{C}}$, la orientación $R$ y la posición del pie
respecto de $\{O\}$ ${^{O}\bar{p}_{2}}$. Usando las transformaciones entre
ternas (ver apéndice \ref{sec:trasfhomogenea}) se puede despejar la posición
del pie respecto de la base de la pata.

\begin{eqnarray*}
    {^{O}\bar{p}_{2}} &=& {^{O}\bar{p}_{C}} + R \left({^{C}\bar{p}_{B_{2}}} + {^{B_{2}}\bar{p}_{2}}\right)\\
    {^{B_{2}}\bar{p}_{2}} &=& R^T \left({^{O}\bar{p}_{2}} - {^{O}\bar{p}_{C}}\right) - {^{C}\bar{p}_{B_{2}}}
\end{eqnarray*}

Se puede escribir una forma general para la posición relativa a la base de una
pata cualquiera $i$ como:

\begin{equation}
  {^{B_{i}}\bar{p}_{i}} = R^T \left({^{O}\bar{p}_{i}} - {^{O}\bar{p}_{C}}\right) - {^{C}\bar{p}_{B_{i}}}
\end{equation}

Con esta posición relativa se calcula el problema inverso de la cadena
cinemática abierta de la misma forma que se hizo para las patas en
transferencia.
