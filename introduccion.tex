\chapter{Introducción}

La robótica móvil tiene todos los problemas de la robótica clásica o industrial,
con el agregado que el robot no estará sólidamente adherido al suelo sino
que, por el contrario, estará libre. Esta libertad traerá problemas de estabilidad
que deberán ser tenidos en cuenta en el planeamiento del movimiento.

El robot que se estudiará tiene la característica de desplazar su cuerpo en
distintas direcciones gracias a la movilidad que le dan sus miembros
articulados. Por esta razón, el planeamiento no se reducirá solamente al
cálculo de las ecuaciones cinemáticas del robot, sino que también necesitará
previamente del análisis de cuál será la posición necesaria del cuerpo y los
miembros. Estas decisiones estarán restringidas a la resolución del problema
de estabilidad.

\section{Antecedentes}

% Trabajos anteriores e historia

La robótica móvil tiene sus orígenes en las máquinas caminantes. Desde el año
480AC con el \emph{Carruaje con caballo de madera}
(\cite{yan2010reconstruction}), han surgido numerosos inventos como la
\emph{Carreta con patas} de Francis Potter (1594 -1678), el \emph{Steam Man} de
Dedericks en 1868 (\cite{steamman}), el \emph{Autómata caminante} eléctrico de
George R. Moore en 1891 (\cite{georgemoore}), el \emph{Moonwalker - Disability
Walker} de Jack Miller en 1966 cuyo movimiento era coordinado mecánicamente.
Ese mismo año Frank y McGhee presentaron su trabajo \emph{Finite state control
of quadruped locomotion} (\cite{mcgee1966}) en el cual se presenta el primer
vehículo con patas totalmente controlado por computadora (cada pata tiene dos
articulaciones actuadas por motores eléctricos con reducción, controlados por
la computadora), solo podía desplazarse de dos formas distintas: caminar y
trotar; y siempre en linea recta.

El estudio de los robots caminantes recién cobro real importancia en la
década del 60 (anteriormente había quedado relegado por que se suponía que era
un sistema de locomoción inferior y por su complejidad de control) gracias
a los trabajos de Bekker de 1960 (\cite{bekker1960}) y 1969 (\cite{bekker1969})
donde demostró que los robots con patas son superiores a los robots con ruedas
en su capacidad de carga vs potencia consumida, en su velocidad en terrenos
adversos, y no menos importante en que son más ecológicos en el sentido que
no destruyen el terreno a su paso (solo dejan huellas discretas).

Este estudio inició una carrera en la investigación de máquinas con múltiples
patas que dejo muchos trabajos en el área (\cite{listonmosher1968},
\cite{cox1970}, \cite{mocci1972}, \cite{schneider1974}, \cite{buckett1977},
\cite{hirose1980}, \cite{brown1982}, \cite{raibert1983}, \cite{russell1983}).

La rama más importante de la robótica móvil con patas es quizá el estudio de
los modos de caminar. Los primeros en usar una matemática para este estudio
fueron Tomovic y Karplus en 1961 (\cite{tomovic1961}) y basaron sus estudios en
los trabajos de Muybridge de 1899 (\cite{muybridge1899}) y 1901
(\cite{muybridge1901}), quien mediante secuencias de fotos identificó los modos
de caminar de distintos cuadrúpedos y bípedos. En 1967, Hildebrand
(\cite{hildebrand1967}) introdujo algunos conceptos y métodos importantes para
el estudio de los modos de caminar, que fueron extendidos posteriormente por
McGhee en 1968 (\cite{mcgheeandfrank1968}), mejorando estos métodos matemáticos dando
una base más fuerte para el estudio. Esta matemática permitió la clasificación
de los distintos modos de caminar y el estudio de estabilidad de los mismos.

En 1988 Waldron y Song en su libro \emph{Machines that walk}
(\cite{MachinesThatWalkWaldron}) utilizan esta matemática para el diseño de un
controlador para un vehículo caminante de 6 patas asegurando la estabilidad
estática del mismo.

Esta ultima es quizás la aplicación más interesante de un robot con patas:
transporte de personas o cargas. Actualmente en este área existen múltiples
diseños exitosos, como el BigDog de Boston Dynamics (\cite{bigdog}) que es el
robot cuadrúpedo más avanzado que existe en la actualidad, o como el ATHLETE
(\cite{athlete}) que fue diseñado como un híbrido para exploración extra
terrestre llevándose lo mejor de los dos mundos: velocidad y agilidad.

Otra rama importante de aplicación de robots caminantes es la inspirada en la
naturaleza. Estos robots mayormente se usan en investigación sobre la
fisionomía y los modos de caminar de animales e insectos (\cite{1511218},
\cite{5650200}, \cite{rise}, \cite{lauron}).

\section{Presentación del estudio} 

En el presente trabajo se estudiará la movilidad de un robot móvil
caminante de 6 grados de libertad de forma de poder diseñar un sistema
distribuido de control de movimiento.

El movimiento de un robot caminante implica un planeamiento más
allá de la trayectoria del cuerpo. Es necesario primero diagramar cuáles serán
las patas, y cuáles las posiciones de apoyo de los pies, que soportarán al
cuerpo a lo largo del movimiento. Se presentará entonces los modos de caminar o
\emph{gaits} que son las secuencias de posiciones del cuerpo y los miembros que
permitirán que el robot camine.

Sabiendo cuáles son las posiciones necesarias del cuerpo se hará un estudio de
estabilidad de estas posiciones definiendo un método de medición como parámetro
de selección entre distintos \emph{gaits}.

Antes de calcular la cinemática del robot es necesario definir cómo esta
formado. Se hará un estudio de los grados de libertad necesarios en cada
miembro y de cuál es la cantidad óptima de miembros para que el robot se mueva
como se desea.

Sabiendo la morfología del robot y los distintos puntos por los cuales tiene
que pasar el centro de gravedad del cuerpo se debe hacer un planeamiento de la
trayectoria del mismo. Se hará este calculo cinemático tratando al cuerpo y
las patas apoyadas como un robot paralelo. Las patas que se encuentren
levantadas del piso deberán adelantarse para ser usadas en la siguiente
translación del cuerpo. En este caso cada pata será tratada como un robot serie
y se hará un calculo cinemático del efector final, el pie, utilizando los
parámetros de Denavit-Hartenberg. Previamente como introducción a los problemas
de un robot paralelo se hará el mismo estudio para una máquina más sencilla con
3 grados de libertad.

Para probar los cálculos cinemáticos se presenta un visualizador gráfico en
computadora que permitirá aplicar la generación de \emph{gait} y trayectoria con
distintos parámetros. Se explicará además como fue diseñado el software para
poder utilizar el mismo código como controlador de un robot real.

Finalmente se utilizarán dos plataformas robóticas donde se mostrará la
versatilidad del sistema y se probarán los algoritmos de movimiento. La primera
plataforma que se utilizará será completamente diseñada como parte de este
trabajo; la segunda será una plataforma comercial que cumpla con los
requerimientos de movilidad.

\newpage
