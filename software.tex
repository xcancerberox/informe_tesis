\chapter{Software}\label{sec:software}

\section{Software de control y visualización}

Para lograr un sistema distribuido con la finalidad de desarrollar y probar
modos de caminar se definieron actores con características y trabajos
particulares:

\begin{itemize}
    \item {\bf Visualizador:} permite visualizar en un entorno 3D una
        configuración o una secuencia de configuraciones del robot.\\
    \item {\bf Generador de ángulos:} se encarga de convertir configuraciones
        del robot en ángulos aplicables a las articulaciones utilizando el
        problema inverso y la morfología del robot.\\
    \item {\bf Ejecutor:} es el que se encarga en el modelo físico del robot de
        convertir los ángulos en acciones de los motores.\\
    \item {\bf Control remoto:} interfaz de comandos básicos de movimiento como
        ser adelante, atrás, giro derecho, giro izquierdo. Se encarga de
        convertir estos comandos en configuraciones del robot utilizando
        generadores de trayectorias.\\
    \item {\bf Interfaz de comandos:} puede generar todos los tipos de mensajes
        que los distintos actores utilizan. Mayormente utilizado para
        desarrollo y testeo.\\
    \item {\bf Generador de movimientos:} utilizado para movimientos guionados.
        Consta de uno o más generadores de trayectorias compaginados para
        generar un movimiento fluido.\\
\end{itemize}

Conectando estos actuadores se pueden armar distintos escenarios de utilización
del código. En la figura \ref{fig:software_escenario_1} se muestra la etapa de
desarrollo de los modos de caminar.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/software_escenario_1.pdf}
        \caption{Escenario de ejecución del software. Generador de movimiento
        entregando configuraciones al visualizador. Etapa de desarrollo de los
        movimientos.}
		\label{fig:software_escenario_1}
\end{figure}

En la figura \ref{fig:software_escenario_2} se muestra la conexión de
actuadores utilizada para probar el movimiento en el robot real.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/software_escenario_2.pdf}
        \caption{Escenario de ejecución del software. Control remoto generando
        trayectorias a partir de una interfaz de usuario. Etapa de prueba de
        los movimientos en el robot real.}
		\label{fig:software_escenario_2}
\end{figure}

En la figura \ref{fig:software_escenario_3} se muestra el caso de \emph{debugging},
donde se utiliza una consola de comandos para enviar comandos directo a los
actuadores.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/software_escenario_3.pdf}
        \caption{Escenario de ejecución del software. Consola generando mensajes
        directamente a los actores. Etapa de \emph{debugging}}
		\label{fig:software_escenario_3}
\end{figure}

Estos escenarios permiten que el desarrollo de los modos de caminar o de los
generadores de trayectorias para el cuerpo o las patas, sean íntegramente
diseñados utilizando el entorno 3D, y posteriormente, solo implementando el
ejecutor del robot particular, utilizar el mismo código para el movimiento del
robot.\\

Para lograr esta flexibilidad en la utilización de la herramienta se trabajó
con un sistema de mensajería entre los procesos. Así, lo que define a un
actuador es qué tipo de mensaje recibe como entrada y qué tipo de mensaje
entrega a la salida.\\

\subsection{Sistema de mensajería entre actores}

El sistema de mensajería entre actores define un tipo de mensaje y un
\emph{payload} (datos del mensaje).
Los tipos de mensajes pueden ser tres:

\begin{itemize}
    \item {\bf RobotConfiguration}: utiliza como \emph{payload} una estructura
        que contiene ialización del la posición y rotación del cuerpo, más la
        posición de todas las puntas de las patas. Esta estructura es
        serializada a partir del objeto RobotConfiguration. 

    \item {\bf Angles}: utiliza como \emph{payload} una lista comprendida por
        seis listas de tres ángulos. Se utiliza una serialización de una lista
        standard de Python.

    \item {\bf Configuration}: el \emph{payload} es particular de cada actor. Permite
        intercambio de configuraciones como ser posición de la cámara en el
        visualizador, configuración de límites en el robot, etc.
\end{itemize}

Para la implementación de este sistema de mensajería se utilizó el proyecto ZeroMQ
(\cite{link:zeromq}) permitiendo que los actores no sólo corran en procesos
distintos, sino que puedan a su vez correr en distintas computadoras conectadas a
una red común.\\

ZMQ brinda una capa de abstracción del sistema operativo generando internamente
los canales de comunicación por los cuales se realiza el intercambio de
información (\emph{socket}) y mostrando una API (\emph{Application Programming
Interface} al usuario basada en modelos de interconexión (documetación del
proyecto \cite{link:zeromqdoc}). Algunos modelos de interconexión son el
\emph{Request-Reply} utilizado para interacción cliente-servidor básica; el
\emph{Push-Pull} utilizado, por ejemplo, para distribución de trabajo entre
procesos; pero el que se utiliza en este trabajo es el
\emph{Publisher-Subscriber} (figura \ref{fig:zmq_pub_sub}, tomada de la
documentación del proyecto \cite{link:zeromqdoc}) que permite que un proceso
publique un mensaje y ese mensaje le llegue a todos lo procesos que estén
interesados en él.\\

\begin{figure}[H]
  \centering
		\includegraphics[width=0.45\textwidth]{imag/eps/zmq_pub_sub.pdf}
        \caption{Modelo Publisher-Subscriber de la biblioteca ZMQ}
		\label{fig:zmq_pub_sub}
\end{figure}

Los actuadores utilizan este modelo ya sea solo escuchando mensajes con un
socket ZMQ de tipo \emph{Subscriber}, solo enviando mensajes con un socket ZMQ
de tipo \emph{Publisher}, o ambos.

\subsection{Actores}

La mayoría de los actores fueron desarrollados en Python 3.7
(\cite{link:pythontressiete}) utilizando distintas bibliotecas que se
aclaran en cada caso. El código a su vez se divide en distintos módulos y
paquetes para facilitar la distribución.

\subsubsection{Visualizador}

En la figura \ref{fig:diagrama_flujo_visualizador} se puede ver un diagrama de
flujo general del funcionamiento del actor de visualización.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/diagrama_flujo_visualizador.pdf}
        \caption{Diagrama de flujo del visualizador.}
		\label{fig:diagrama_flujo_visualizador}
\end{figure}

El visualizador esta formado por una ventana, una escena, una cámara y un
receptor de mensajes. 

La ventana es creada utilizando la biblioteca Pyglet (\cite{link:pyglet}) que a
su vez permite la interacción del usuario con el visualizador y el
manejo de tiempos con la utilización de eventos.

La escena es un conjunto de objetos del tipo \emph{Drawing}. Estos objetos
tienen la particularidad de tener un método \emph{draw} que utiliza primitivas
y transformaciones OpenGL para posicionar el objeto en el entorno 3D sesenta
veces por segundo.

La cámara permite enfocar la escena desde distintos ángulos, para ser mostrada
en la ventana. Se implementaron dos cámaras: una cámara libre, implementada en
el objeto \emph{FreeCamera}, que permite recorrer la escena de forma libre; y
una cámara que persigue al robot y que permite rotar al rededor, implementada
en \emph{FollowingCamera}.


\subsubsection{Generador de ángulos}

En la figura \ref{fig:diagrama_flujo_generador_de_angulos} se puede ver un
diagrama de flujo general del funcionamiento del actor.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.4\textwidth]{imag/eps/diagrama_flujo_generador_de_angulos.pdf}
        \caption{Diagrama de flujo del generador de ángulos.}
		\label{fig:diagrama_flujo_generador_de_angulos}
\end{figure}

Utilizando el modelo cinemático del robot (sección \ref{sec:modelocinematico})
y un receptor de mensajes \emph{RobotConfiguration}, calcula el problema
inverso del robot y publica esos ángulos con el mensaje \emph{Angles}.

El modelo cinemático del robot está formado por la configuración morfológica
del robot, más un modelo para cada una de las cadenas cinemáticas que forman
las patas. Para el cálculo cinemático se utiliza la morfología para llegar a la
posición relativa del actuador final de cada pata.

El modelo de la cadena cinemática que forma la pata utiliza las cuentas
descriptas en la sección \ref{sec:estudiocinematico}, resolviendo los ángulos
para una posición relativa de el actuador final de la cadena cinemática.

\subsubsection{Ejecutor}

Se desarrollaron dos ejecutores: uno para el modelo A (sección
\ref{sec:mecanica_modeloA}) en python, y uno para el modelo B (sección
\ref{sec:mecanica_modeloB}) en C++ para Arduino.

El primero se desarrolló utilizando el mismo sistema de mensajes, escuchando
mensajes del tipo \emph{Angles}. Los ángulos son enviados a los
distintos miembros por el ejecutor mediante el bus I2C.
Cada pata tiene una dirección en el bus y recibe 3 comandos de seteo de ángulo.
Los controladores distribuidos, programados en C utilizando la librería
\emph{AVR-libc} y el compilador \emph{GCC}, ejecutan un \emph{firmware} que en su lazo
principal parsea los comandos de I2C para la configuración de los ángulos
máximo y mínimo de cada articulación y el ángulo deseado. 

\begin{figure}[H]
  \centering
		\includegraphics[width=0.4\textwidth]{imag/eps/diagrama_flujo_ejecutor_python.pdf}
        \caption{Diagrama de flujo del ejecutor de ángulos escrito en python.}
		\label{fig:diagrama_flujo_ejecutor_python}
\end{figure}

En la figura \ref{fig:diagrama_flujo_ejecutor_C} se puede ver un diagrama
esquemático del segundo ejecutor desarrollado en C para el modelo B (sección
\ref{sec:mecanica_modeloB}). Este ejecutor necesita un receptor de mensajes del
tipo \emph{Angles}, en python, que convierte los mensajes en comandos UDP al
robot. La segunda parte es la que se muestra en el diagrama y corre en el
robot. Recibe los comandos UDP los ejecuta en el robot. 

\begin{figure}[H]
  \centering
		\includegraphics[width=0.4\textwidth]{imag/eps/diagrama_flujo_ejecutor_C.pdf}
        \caption{Diagrama de flujo del ejecutor de ángulos escrito en C.}
		\label{fig:diagrama_flujo_ejecutor_C}
\end{figure}

\subsubsection{Control remoto}

En la figura \ref{fig:diagrama_flujo_control_remoto} se muestra un diagrama de
flujo general del funcionamiento del control remoto.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.8\textwidth]{imag/eps/diagrama_flujo_control_remoto.pdf}
        \caption{Diagrama de flujo del control remoto.}
		\label{fig:diagrama_flujo_control_remoto}
\end{figure}

Utilizando el generador de trayectorias y un generador de trayectorias
de transición, permite convertir comandos básicos de movimiento (Atrás,
Adelante, Giro CW, Giro CCW) en trayectorias de movimiento.

Empezando en una posición relajada del robot, cuando recibe un comando de
movimiento genera un \emph{gait} que cumpla con ese comando. Para poder empezar con el
movimiento del \emph{gait} genera una trayectoria de transición entre la posición
relajada del robot y el primer paso del \emph{gait}. 

Una vez inicializado el \emph{gait} continua moviéndose hasta recibir un comando de
detención. En ese momento genera otra trayectoria de transición entre el último
paso efectuado por el \emph{gait} y la posición de reposo de las patas.

\subsubsection{Interfaz de comandos}

En la figura \ref{fig:diagrama_flujo_interfaz_de_comandos} se puede ver el
diagrama de flujo de la interfaz de comandos.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.4\textwidth]{imag/eps/diagrama_flujo_interfaz_de_comandos.pdf}
        \caption{Diagrama de flujo de la interfaz de comandos por consola.}
		\label{fig:diagrama_flujo_interfaz_de_comandos}
\end{figure}

Es una consola en python utilizando la biblioteca \emph{Cmd} que permite
generar todos los tipos de mensajes, permitiendo testeo y \emph{debugging} del código.

\begin{lstlisting}
diloboderusabderus (master)$ python bin/console.py
(Cmd) help

Documented commands (type help <topic>):
========================================
EOF   help  quit         send_app_config
exit  q     send_angles  send_robot_configuration

(Cmd) help send_angles
Send Angles. [[90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0], [90, 10, 0]]
(Cmd) help send_app_config
Send app config. {"app_config_key": app_config_value}
(Cmd) help send_robot_configuration
Send RobotConfiguration. [[0, 0, 0, 1], [0, 0, 0, 1], [1, 2, 3], [[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]]
\end{lstlisting}

\subsubsection{Generador de movimientos}

Utilizando uno o más generadores de trayectorias y uno o más generadores de
trayectorias de transición, se orquesta un movimiento complejo generando todas
las configuraciones intermedias.  

Se definieron 3 generadores de movimiento: un movimiento lineal puro en el
modulo \emph{forward\_move.py} (figura \ref{fig:diagrama_flujo_forward_move}),
un movimiento de \emph{spinning} puro en \emph{spinning\_move.py} (figura
\ref{fig:diagrama_flujo_spinning_move}) y un movimiento complejo que se mueve
linealmente, luego gira y vuelve a moverse linealmente, en
\emph{complex\_move.py} (figura \ref{fig:diagrama_flujo_complex_move}).

\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/diagrama_flujo_forward_move.pdf}
        \caption{Diagrama de flujo de un movimiento orquestado hacia delante.}
		\label{fig:diagrama_flujo_forward_move}
\end{figure}
\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/diagrama_flujo_spinning_move.pdf}
        \caption{Diagrama de flujo de un movimiento orquestado de giro.}
		\label{fig:diagrama_flujo_spinning_move}
\end{figure}
\begin{figure}[H]
  \centering
		\includegraphics[width=0.7\textwidth]{imag/eps/diagrama_flujo_complex_move.pdf}
        \caption{Diagrama de flujo de un movimiento orquestado complejo, con un
            movimiento hacia delante, un giro, y otro movimiento hacia delante.}
		\label{fig:diagrama_flujo_complex_move}
\end{figure}


\subsection{Clases destacadas}

En esta sección se explicarán las clases destacadas que se utilizan para
lograr un entendimiento más profundo del software.

\subsubsection{Cartesian Coordinates}\label{sec:cartesiancoordinates}

Esta clase pertenece al paquete \emph{DiloboderusAbderus}, modela un set de
coordenadas cartesianas posicionado en otro set de coordenadas. La posición del
set de coordenadas se define por su posición y su rotación respecto al set de
coordenadas externo.

Define los métodos \emph{absolute\_to\_relative\_position} y
\emph{relative\_to\_absolute\_position} que utilizan matrices de rotación y
translación para convertir posiciones definidas en un set de coordenadas al
otro (según se define en el apéndice \ref{sec:trasfhomogenea}).

\subsubsection{Modelo cinemático}\label{sec:modelocinematico}

El modelo cinemático del robot está representado en la clase \emph{Hexapod} del
paquete \emph{DiloboderusAbderus}.

Esta clase hereda de la clase \emph{CartesianCoordinates} (sección
\ref{sec:cartesiancoordinates}) lo que le agrega los métodos de transformación
de posiciones relativas a absolutas, y viceversa.

Utilizando la morfología del robot instancia seis modelos de pata,
representados en la clase \emph{Leg}. El método \emph{inverse\_kinematics}
calcula la cinemática inversa del robot hexapod utilizando el método del mismo
nombre de cada una de las instancias de \emph{Leg} con la posición relativa de
la punta de la pata correspondiente, devolviendo un set completo de ángulos
como solución del problema inverso.

\subsubsection{Modos de caminar}

Los modos de caminar están representados cada uno por una clase
(\emph{ForwardWaveGait}, \emph{TripodGait}, \emph{SpinningWaveGait}). Todas
ellas heredan de una clase padre llamada \emph{Gait} que define los métodos y
atributos comunes a todos. 

El método  \emph{get\_config} es un generador de python. Permite llamarlo
recursivamente y este entrega, por cada llamada, un punto del \emph{gait} hasta que
se termine. Este método solo necesita que en la clase particular de cada \emph{gait}
se defina la posición relativa de las patas para la posición de inicio de
soporte y de inicio de transferencia, junto con la fase de cada pata y el beta
del \emph{gait}.

\subsubsection{Generador de trayectoria}

Al igual que el \emph{get\_config} de los \emph{gaits}, el \emph{get\_config} del
generador de trayectorias esta implementado como un generador de python que
genera todas las configuraciones intermedias del robot. La cantidad de
configuraciones intermedias puede ser controlada con el parámetro
\emph{step\_multiplier}.

En el diagrama de la figura \ref{fig:diagrama_flujo_generador_trayectorias} se
puede ver como el generador, primero toma una nueva configuración del \emph{gait} y
luego genera una trayectoria entre la configuración actual y la nueva
configuración. La trayectoria es generada tanto para las patas en translación
como para las patas en soporte. Para facilitar el uso del visualizador también
se genera la trayectoria del cuerpo de forma inversa a la de las patas de
soporte.

\begin{figure}[H]
  \centering
		\includegraphics[width=0.6\textwidth]{imag/eps/diagrama_flujo_generador_trayectorias.pdf}
        \caption{Diagrama de flujo del generador de trayectorias.}
		\label{fig:diagrama_flujo_generador_trayectorias}
\end{figure}

Se configuran nuevas trayectorias cada vez que una pata pasa de modo soporte a
modo transferencia y viceversa. Cada trayectoria es generada con la cantidad de
pasos intermedios para que el movimiento sea continuo y las trayectorias no se
solapen.

\subsubsection{Generador de trayectoria de transición}

Para poder conectar un \emph{gait} con otro, o la posición inicial del robot con la
posición necesaria para empezar un \emph{gait}, se utiliza el generado de trayectorias
de transición.

En esencia actúa de la misma forma que el generador de trayectoria con la
diferencia de no tener un \emph{gait} asociado, sino que, genera una trayectoria
arbitraria entre dos configuraciones del robot.

\section{Sistema de control embebido}

El sistema de control del robot Modelo A fue desarrollado en dos partes. Un
controlador central con la capacidad de ejecutar código phyton y 6
controladores secundarios capaces de recibir comandos sencillos. 

\begin{figure}[H]
  \centering
		\includegraphics[width=0.6\textwidth]{imag/eps/sistema_embebido_modeloA.pdf}
        \caption{Diagrama de interconexión entre los controladores.}
		\label{fig:sistema_embebido_modeloA}
\end{figure}


\subsection{Controlador principal}

Para poder ejecutar el código en el robot modelo A (sección
\ref{sec:mecanica_modeloA}) se eligió un dispositivo que permita la ejecución
de código python.  Se utilizó una placa \emph{Beagleboard} con un sistema operativo
GNU/Linux. Esta placa tiene un microcontrolador \emph{OMAP3530} de Texas Instruments
(\cite{texasinstruments}) con las características necesarias para poder
instalar Linux.

El sistema está formado por:

\begin{itemize}
  \item Bootloaders con la configuración de los pines necesarios para la placa.
  \item Kernel Linux con soporte para la placa, comunicación I2C y manejo de
    GPIO.
  \item File System base con python y bibliotecas matemáticas necesarias para
    ejecutar el controlador.
\end{itemize}

\subsubsection{Bootloader}

El booteo de la placa implica 3 aplicaciones distintas (figura
\ref{fig:beagleboardboot}).

\begin{figure}[H]
  \centering
		\includegraphics[width=0.45\textwidth]{imag/eps/beagleboardboot.pdf}
		\caption{Etapas del booteo de la placa.}
		\label{fig:beagleboardboot}
\end{figure}

La aplicación de {\bf booteo de bajo nivel} se llama ROMCODE, es un segmento de
código ya grabado en la ROM del microcontrolador \emph{OMAP3530} que se encarga de
hacer las inicializaciones primordiales. Entre estas se encuentra el inicio de
los periféricos y de la SRAM. Una vez inicializado busca entre los dispositivos
de booteo (Serial, NAND, eMMC, SD y USB) una imagen válida del bootloader de
primer nivel.\\

El {\bf bootloader de primer nivel} es el x-loader. Este lo otorga la misma
empresa que fabrica el chip (Texas Instruments) y es una versión reducida del
bootloader \emph{Das UBoot}. Este es el encargado de setear los multiplexores
de los pines, inicializar los clocks y la memoria SDRAM cargando en esta el
bootloader de segundo nivel.\\

El {\bf bootloader de segundo nivel} queda a elección del usuario. Este es el
encargado de terminar de inicializar los periféricos del microcontrolador y
cargar el kernel en la memoria SDRAM.

En este caso se utilizó una versión completa de UBoot cross compilada desde el
código fuente para poder configurar los pines del microcontrolador como
entrada/salida.

\subsubsection{Kernel Linux}

El kernel es el encargado de la interacción de las aplicaciones en espacio de
usuario y el hardware. Utilizando las llamadas a sistema las aplicaciones de
espacio de usuario puede hacer uso de los periféricos y de los distintos
recursos del hardware.

Se utilizó un kernel 2.6.32 con los parches particulares de omap3 y
beagleboard. Para este proyecto fue necesario cross compilar el kernel desde su
código fuente para agregar el soporte de comunicación I2C, manejo de GPIOs y
\emph{dongle wifi}.

Para poder acceder desde espacio de usuario al dispositivo I2C se utilizó el
modulo \emph{i2c-dev} que crea una interfaz de usuario dentro del file system
virtual del kernel, \emph{sysfs}. Se escribió una biblioteca en python que
maneja estos archivos para enviar información a través de la interfaz I2C de la
placa.

El módulo de control de GPIOs permite, también desde \emph{user space} a través
de \emph{sysfs}, controlar el modo de los pines y la lectura o escritura de los
mismos.

\subsubsection{Root filesystem}

para completar la instalación del SO se armó un root filesystem con las
herramientas básicas, Python, la librería matemática Numpy y herramientas para
la configuración de red. Para esta tarea se utilizó el entorno de cross
compilación OpenEmbedded.


\subsection{Control de cada miembro}

El actuador del tipo \emph{Ejecutor} que corre en el controlador principal
entrega los ángulos a los distintos miembros por el modulo distribuidor de
ángulos mediante el bus I2C.  Cada pata tiene una dirección en el bus y recibe
3 comandos de seteo de ángulo.

Los controladores distribuidos, programados en C utilizando la librería
\emph{AVR-libc} y el compilador \emph{GCC}, ejecutan un firmware que en su lazo
principal parsea los comandos de I2C para la configuración de los ángulos
máximo y mínimo de cada articulación y el ángulo deseado. El \emph{duty cycle}
de los PWM se calcula en base a estos 3 parámetros.

Los PWMs son generados utilizando un único timer del microcontrolador como se
muestra en la figura \ref{fig:generacionpwm}.

\begin{figure}[ht]
  \centering
  \newsavebox{\myimagez}% Box that will store image
  \begin{lrbox}{\myimagez}
    \input{imag/tex/generacion-pwm.tex}
  \end{lrbox}
  \resizebox{0.45\textwidth}{!}{\usebox{\myimagez}}% Resize box
		\caption{Generación de los PWM.}
		\label{fig:generacionpwm}
\end{figure}

Como se mencionó se utiliza una señal PWM como señal de control. El período de
la señal de control es estándar de $20~\text{ms}$. La duración del pulso activo indica
el ángulo de giro del motor. En general un ancho de pulso de $1.5~\text{ms}$ coloca al
servo en su posición neutral. Cada servo tiene sus márgenes de operación, que
se corresponden con el ancho del pulso máximo y mínimo que el servo entiende y
que típicamente son $1~\text{ms}$ y $2~\text{ms}$.  Los PWM se generan de forma cíclica cada
$\frac{1}{3}$ del período total del PWM utilizando una máquina de estados que
decide cual es el siguiente y configura una interrupción por comparación del
timer con el \emph{duty cycle} calculado.

Por otro lado se utiliza una interrupción externa del microcontrolador para
sincronizar el cambio de ángulos. De esta forma se asegura que el movimiento de
todos los motores es sincronizado.
