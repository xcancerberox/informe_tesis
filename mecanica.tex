\chapter{Mecánica}

Como plataformas de testeo se utilizaron dos robots. Uno, el modelo A,
desarrollado íntegramente y el otro, el modelo B, utilizando una plataforma
comercial.

\section{Modelo A}\label{sec:mecanica_modeloA}

El prototipo fue diseñado para poder probar el cálculo cinemático y los \emph{gaits}.
La morfología del robot fue elegida según varios criterios: desplazamiento,
grados de libertad, manufacturabilidad. En el capitulo \ref{cap:gaits} se
presentaron los cálculos ya suponiendo una estructura en la cual las patas
están igualmente distribuidas a los costados del robot. Esta es la forma en la
que el robot obtiene su mayo movilidad lineal. 

En el capitulo \ref{cap:gdl} se demostró que la cantidad óptima de patas para
obtener los grados de libertad buscados es 6, con lo cual queda definida la
forma final del robot. En las siguientes secciones se presentarán algunas
decisiones que llevaron a que la fabricación fuese sencilla.

\subsection{Patas}

Para conseguir los 3 grados de libertad necesarios en la forma que fue
explicada en la sección \ref{cap:gdl} se cotejaron varios diseños.

El primer diseño es el presentado en el trabajo \cite{yantogo} donde se
utiliza una pata de 3 grados de libertado donde los actuadores se encuentran
en el cuerpo y todas las articulaciones son manejadas por cables
\ref{fig:cable-driven_leg}.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/cable-driven_leg.pdf}
		\caption{Pata con articulaciones manejadas por cables.}
		\label{fig:cable-driven_leg}
	\end{center}
\end{figure}

Teniendo todos los actuadores en la base se consigue que la mayor masa esté
lejos de la punta mejorando la dinámica del movimiento de translación de la
pata y, al tener una pata mas estilizada, se consigue una mayor movilidad de los
miembros. También permite utilizar motores más voluminosos que si se colocasen
sobre las articulaciones.
Se descartó este mecanismo por su complejidad de fabricación y se propuso una
alternativa que mantuviese los actuadores cerca del cuerpo. La alternativa
propuesta es la de la figura \ref{fig:pantograph_leg} donde el movimiento de
rotación es transmitido a la articulación por medio de una estructura tipo
pantógrafo.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/pantograph_leg.pdf}
		\caption{Patas con articulaciones manejadas por estructuras tipo pantógrafo.}
		\label{fig:pantograph_leg}
	\end{center}
\end{figure}

El segundo diseño es el más común en robots comerciales. Cada articulación está
formada por un actuador rotatorio utilizando el cuerpo del actuador y la parte
móvil para unir los eslabones de la cadena cinemática.

En la figura \ref{fig:axis_leg} se puede ver un prototipo de esta pata que
utiliza servomotores como actuadores. Uno de los eslabones se agarra al cuerpo
del servomotor y el siguiente al eje. Este diseño hace que las patas sean más
voluminosas que en el diseño anterior pero la fabricación se vuelve muy
sencilla.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/axis_leg.pdf}
		\caption{Pata sostenida por los ejes de los actuadores.}
		\label{fig:axis_leg}
	\end{center}
\end{figure}

El mayo problema que se encontró a este diseño es la carga sobre el eje del
motor. El peso del robot genera, además de la fuerza de torsión sobre el eje,
una fuerza perpendicular, que hará que la estructura del motor y el movimiento
del mismo se degrade con el tiempo. Para resolver este problema se planteó
liberar el eje de esta carga utilizando un doble apoyo sobre rodamientos. Este
diseño será explicado más adelante con el actuador elegido.

\subsubsection{Selección del actuador}

Los requerimientos para el actuador son:

\begin{itemize}
  \item Movimiento giratorio.
  \item Controlado eléctricamente.
  \item Alimentación eléctrica (utilizar actuadores neumáticos complicaría el
    armado).
  \item Reducido volumen.
  \item Alto torque.
\end{itemize}

Estas características llevaron a la elección de un servo motor analógico como
actuador \ref{fig:servomotor}.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width =0.3\textwidth]{imag/servomotor.pdf}
		\caption{Servomotor HiTec.}
		\label{fig:servomotor}
	\end{center}
\end{figure}

Estos dispositivos utilizan un lazo de realimentación negativa para mantener la
posición (lineal, angular, etc.) cerca del valor deseado. Cualquier diferencia
entre estas genera una señal de error que es amplificada y se utiliza para
corregirla. El servo utilizado en este trabajo cae en la categoría de RC servo
y consiste en un motor de corriente continua acoplado a una caja de engranajes
cuyo ángulo de rotación es controlado por un lazo proporcional. Para el sensado
del ángulo generalmente se utiliza un potenciómetro que se acopla al eje de
salida de la caja. El valor del ángulo deseado lo recibe como una señal
PWM\footnote{\textbf{P}ulse \textbf{W}idth \textbf{M}odulation, o Modulación
por Ancho de Pulso, donde la información se transmite en el ancho del pulso
activo de una señal cuadrada.} por un cable que se conecta con el exterior cuyo
color depende del fabricante. En su otro extremo el cable está conectado a la
entrada donde existe un conversor de pulsos a tensión cuya salida se compara
con la tensión realimentada por el potenciómetro. Todos estos elementos se
encuentran dentro del encapsulado de un servomotor. Aparte del cable de señal
hay otros dos cables de alimentación.

Los servos son muy populares en robótica, aeromodelismo y hobbismo tanto para
autos como para barcos. La mayoría pueden girar entre $90^{\circ}$ y
$120^{\circ}$, habiendo algunos que lo pueden hacer $360^{\circ}$ o más para
aplicaciones particulares. Aún así, en general no pueden rotar continuamente y
tienen una traba mecánica para limitarlos.

Los servomotores se dividen principalmente entre analógicos (estándar) y
digitales. Ambos tienen los mismos motores, engranajes, potenciómetros y
encapsulados. No hay diferencia en la forma en la que el servo es controlador
por el usuario. La principal diferencia radica en como el motor es manejado por
el controlador. La principal ventaja de los servos digitales es que responden
de forma más rápida (300Hz contra 50Hz de los analógicos) aunque a costa de un
mayor consumo y un mayor precio. Los servomotores digitales aparte tienen la
ventaja de que pueden ser programados para cambiar parámetros de configuración.
Éstos incluyen: la dirección de rotación, puntos extremos y de centro, opción
de \emph{failsafe}, velocidad, etc. 

Si se desea que el servo mantenga una dada posición en el tiempo entonces
deberá mantenerse la señal de pulsos en su entrada. Si esta se anula entonces
el servo carecerá de referencia en la entrada y no ejercerá torque para
mantener la posición.

Una característica de los servos analógicos es la existencia de una banda
muerta o \emph{dead band}. La misma se mide en $\mu s$ y se define como lo que
se puede mover el eje sin que se genere una respuesta, es decir, sin que el
controlador envíe un comando para mantener la posición. Es decir, cuanto más
pequeña la banda mejor se podrá mantener una posición. Otra definición de
\emph{dead band} dice que es el mínimo ancho de pulso de la señal de control
que genera un cambio en la posición. De esta manera puede verse como una
resolución. Por su parte, los servos digitales mejoran este aspecto pues tiene
una \emph{dead-band} nula. A veces para evitar un gastos de energía
innecesarios (pues el motor nunca se detendría intentando corregir cualquier
pequeña diferencia) se le programa un valor diferente de cero.

En la sección \ref{sec:software} se explicará la forma de controla de estos
actuadores.

\subsubsection{Diseño de la pata}

Para el diseño de la pata se utilizó una aplicación de diseño asistido por
computadora (CAD). El primer diseño que se propuso fue tomar cada servo y
hacerle un encapsulado que le agregase un apoyo en el eje de movimiento.

En la figura \ref{fig:servo_encapsulado} se puede ver el diseño propuesto.
Utilizando 4 placas rectangulares del ancho del servo se lo encierra agregando
en la parte inferior un eje fijo y sobre la superior un eje móvil con
rodamiento que gira solidario al eje del servomotor.
\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/servo_encapsulado.pdf}
		\caption{Servo con encapsulado.}
		\label{fig:servo_encapsulado}
	\end{center}
\end{figure}
Para armar la cápsula es necesario realizar agujeros roscados en el canto de
las piezas para poder unirlas en la forma que se muestra. Este hecho hace que el
armado sea complejo.

Utilizando este servo encapsulado se puede armar una pata como la que se ve en
la figura \ref{fig:mec_pata_completa_v1} donde se unen las cápsulas con
barras paralelas para formar la pata. Se agrega un link final con un apoyo
circular para que no resbale.
\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_completa_v1.pdf}
		\caption{Primera versión de la pata.}
		\label{fig:mec_pata_completa_v1}
	\end{center}
\end{figure}

Por la complejidad de armado y el peso final de la pata se busco otro diseño
que llevara menos material y tiempo de armado. La propuesta fue dejar la
primer articulación igual, utilizando el servo con encapsulado, y unir las
articulaciones 2 y 3 en una misma pieza. En la figura
\ref{fig:mec_pata_link1v3} se ve la pieza que une la articulación doble con el
servo encapsulado. A su vez el servo encapsulado es el que se agarra al cuerpo.

\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_link1v3.pdf}
		\caption{Pieza para unir el servo encapsulado con la articulación doble.}
		\label{fig:mec_pata_link1v3}
	\end{center}
\end{figure}

En la figura \ref{fig:mec_pata_completa_v2} se puede ver la pata completa con
el nuevo diseño. Además de poner las dos articulaciones en una misma pieza se
sacaron los servos y se pusieron de costado, alargando los ejes y apoyándolos
en dos rodamientos. Se agrega una sección final, mucho más fina que el diseño
original, que apoya mediante una bola de silicona contra la superficie de
desplazamiento.

\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_completa_v2.pdf}
		\caption{Segunda versión de la pata.}
		\label{fig:mec_pata_completa_v2}
	\end{center}
\end{figure}

Las mejoras de este diseño son sustanciales ya que la cantidad de material
utilizado se redujo y las uniones roscadas se sustituyeron por uniones con
tornillo pasante y tuerca. Para mejorar el peso de la pata se redujo el
material utilizado en la articulación doble como se puede ver en la figura
\ref{fig:mec_pata_art2v3}. La figura \ref{fig:mec_pata_completa_v3} muestra un
modelo completo de pata con las modificaciones introducidas.

\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_art2v3.pdf}
		\caption{Articulación doble con reducción de masa.}
		\label{fig:mec_pata_art2v3}
	\end{center}
\end{figure}

\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_completa_v3.pdf}
		\caption{Tercera versión de la pata.}
		\label{fig:mec_pata_completa_v3}
	\end{center}
\end{figure}

En la figura \ref{fig:mec_completo_patav3} se presenta un diseño completo del
robot utilizando estas patas. Para reducir más el peso se decidió utilizar la
estructura del cuerpo para sostener y encapsular el servo de la primer
articulación, con lo que la pata se redujo a la figura
\ref{fig:mec_pata_completa_v4} y en la figura \ref{fig:mec_completo_patav4_1} se
puede ver el diseño final. En la siguiente sección se explicará el diseño del
cuerpo.

\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_completo_patav3.pdf}
		\caption{Diseño del robot completo utilizando la tercera versión de la pata.}
		\label{fig:mec_completo_patav3}
	\end{center}
\end{figure}
\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_pata_completa_v4.pdf}
		\caption{Cuarta versión de la pata.}
		\label{fig:mec_pata_completa_v4}
	\end{center}
\end{figure}
\begin{figure}[!hbt]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/eps/mec_completo_patav4_1.pdf}
		\caption{Diseño del robot completo utilizando la cuarta versión de la pata.}
		\label{fig:mec_completo_patav4_1}
	\end{center}
\end{figure}

Debido a los agujeros en el canto del material necesarios para unir las piezas
se eligió un espesor de $4mm$. Como también se necesitaba un material liviano
pero fuerte y que no se deformara se eligió aluminio sobre otras opciones como
plástico o acrílico.

\subsection{Cuerpo}

La forma del cuerpo queda definida por la posición de las patas y el ángulo
efectivo que se quiere lograr con cada una. El parámetro importante en la
posición de las patas es la alineación del eje de rotación de la primera
articulación en las patas de un mismo lado y que la distribución sea
perfectamente simétrica. La distancia entre patas fue definida para lograr un
ángulo efectivo de $60^{\circ}$ en cada una.

En la figura \ref{fig:schem_cuerpo_1} se ve la forma elegida para el cuerpo.
Los servos delanteros y traseros fueron rotados $60^{\circ}$ para lograr la
separación requerida entre las patas sin alargar la zona central del cuerpo.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.35\textwidth]{imag/eps/schem_cuerpo_1.pdf}
		\caption{Esquemático de la forma del cuerpo.}
		\label{fig:schem_cuerpo_1}
	\end{center}
\end{figure}

Las puntas redondeadas en la posición de los servomotores son debido a la forma
de la unión con la siguiente articulación. Los agujeros fueron agregados para
reducir la cantidad de material utilizada en el cuerpo. Para encapsular al servo
como se propuso anteriormente el cuerpo se diseñó en tres capas (figura
\ref{fig:mec_cuerpo_capas}).

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.6\textwidth]{imag/eps/mec_cuerpo_capas.pdf}
		\caption{Diseño en capas del cuerpo.}
		\label{fig:mec_cuerpo_capas}
	\end{center}
\end{figure}

La capa inferior (\ref{fig:mec_cuerpo_capas} izquierda) tiene los agujeros
necesarios para sostener un eje corto fijo. Este eje corto es el mismo que se
utilizaba en la versión encapsulada del servo. La capa del medio
(\ref{fig:mec_cuerpo_capas} centro) es la que sostiene el servo. Tiene un
agujero con la forma del servo para que este no pueda rotar, además el
servo es fijado a la capa utilizando los agujeros de sujeción estándar del
mismo. La capa superior (\ref{fig:mec_cuerpo_capas} derecha) tiene un agujero
sobre el que se fija un rodamiento para que apoye el eje largo que se une al
eje del servo. Las tres capas se unen utilizando varilla roscada de $3mm$ en
los 21 puntos marcados. La varilla roscada además de unir las capas agrega
estructura al cuerpo.

Se planteó en un principio hacer el cuerpo de aluminio pero su elevado costo de
mecanizado hizo que fuese descartado. En cambio se utilizó plástico (placas de
polietileno prensado) y se mecanizó manualmente llevando a un cuerpo más
liviano.

\subsection{Fabricación}

Como ya se mencionó el diseño fue hecho completamente en un sistema CAD. Las
piezas de aluminio para las patas fueron mecanizadas con CNC y retocadas a
mano. En la figura \ref{fig:mec_pata_1} se pueden ver todas las partes que
conforman una pata, incluidos los tornillos y rodamientos necesarios.

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width =0.6\textwidth]{imag/eps/mec_pata_1.pdf}
		\caption{Pata versión 4, partes.}
		\label{fig:mec_pata_1}
	\end{center}
\end{figure}

Gracias al diseño la mayor parte del armado se reduce a unir las piezas con
tornillo y tuerca. Solo una pieza necesita agujeros roscados y es la que une la
primer articulación con la segunda (figura \ref{fig:mec_pata_link1v3}). En las
figuras \ref{fig:mec_pata_armado:a}  y \ref{fig:mec_pata_armado:b} se puede ver la pata
completa sin motores.

\begin{figure}[H]
  \centering
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\mecpatacuatro}
      \begin{lrbox}{\mecpatacuatro}
        \includegraphics{imag/eps/mec_pata_4.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\mecpatacuatro}}
      \caption{Vista lateral}
      \label{fig:mec_pata_armado:a}
  \end{subfigure}
  \begin{subfigure}{0.45\textwidth}
      \centering
      \newsavebox{\mecpatacinco}
      \begin{lrbox}{\mecpatacinco}
	  	  \includegraphics{imag/eps/mec_pata_5.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\mecpatacinco}}
      \caption{Vista superior}
      \label{fig:mec_pata_armado:b}
  \end{subfigure}
  \caption{Armado de la pata versión 4.}
\end{figure}

El cuerpo fue cortado a mano utilizando un taladro de banco con un fresa. Se
utilizó una plantilla para transferir el diseño al material (figura
\ref{fig:mec_cuerpo_3}) y se cortaron las tres piezas al mismo tiempo para
asegurar la alineación de los agujeros (figura \ref{fig:mec_cuerpo_7}).

\begin{figure}[H]
  \centering
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\meccuerpotres}
      \begin{lrbox}{\meccuerpotres}
		    \includegraphics{imag/eps/mec_cuerpo_3.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerpotres}}
		  \caption{Agujereado de las 3 capas.}
		  \label{fig:mec_cuerpo_3}
  \end{subfigure}
  \begin{subfigure}{0.43\textwidth}
      \centering
      \newsavebox{\meccuerposiete}
      \begin{lrbox}{\meccuerposiete}
		    \includegraphics{imag/eps/mec_cuerpo_7.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerposiete}}
		  \caption{Corte con plantilla.}
  		\label{fig:mec_cuerpo_7}
  \end{subfigure}
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\meccuerpocuatro}
      \begin{lrbox}{\meccuerpocuatro}
		    \includegraphics{imag/eps/mec_cuerpo_4.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerpocuatro}}
		  \caption{Capas cortadas.}
  		\label{fig:mec_cuerpo_4}
  \end{subfigure}
  \caption{Corte del cuerpo.}
\end{figure}

Para unir las capas primero se colocaron los ejes cortos en la capa inferior y
los motores en la del centro. Utilizando las varillas roscadas se unieron estas
dos capas como se ve en la figura \ref{fig:mec_cuerpo_5}. Se colocaron los
rodamientos en la capa superior y los ejes largos sobre los servomotores
terminando de unir las tres capas como se ve en la figura
\ref{fig:mec_cuerpo_8}

\begin{figure}[H]
  \centering
  \begin{subfigure}{0.38\textwidth}
      \centering
      \newsavebox{\meccuerpocinco}
      \begin{lrbox}{\meccuerpocinco}
    		\includegraphics{imag/eps/mec_cuerpo_5.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerpocinco}}
		  \caption{Capas inferiores del cuerpo con motores.}
  		\label{fig:mec_cuerpo_5}
  \end{subfigure}
  \begin{subfigure}{0.46\textwidth}
      \centering
      \newsavebox{\meccuerposeis}
      \begin{lrbox}{\meccuerposeis}
		    \includegraphics{imag/eps/mec_cuerpo_6.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerposeis}}
		  \caption{Armado del cuerpo.}
  		\label{fig:mec_cuerpo_6}
  \end{subfigure}
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\meccuerpoocho}
      \begin{lrbox}{\meccuerpoocho}
		    \includegraphics{imag/eps/mec_cuerpo_8.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccuerpoocho}}
	    	\caption{Cuerpo, vista lateral.}
    		\label{fig:mec_cuerpo_8}
  \end{subfigure}
  \caption{Armado del cuerpo.}
\end{figure}

En la figura \ref{fig:mec_completo_1} se ve el conjunto completo de partes que
se utilizó para el armado del robot. El prototipo final armado se puede ver en
la figura \ref{fig:mec_completo_2}.

\begin{figure}[H]
  \centering
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\meccompletouno}
      \begin{lrbox}{\meccompletouno}
		    \includegraphics{imag/eps/mec_completo_1.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccompletouno}}
      \caption{Partes del prototipo final.}
      \label{fig:mec_completo_1}
  \end{subfigure}
  \begin{subfigure}{0.4\textwidth}
      \centering
      \newsavebox{\meccompletodos}
      \begin{lrbox}{\meccompletodos}
		    \includegraphics{imag/eps/mec_completo_2.pdf}
      \end{lrbox}
      \resizebox{\textwidth}{!}{\usebox{\meccompletodos}}
      \caption{Partes del prototipo final.}
      \label{fig:mec_completo_2}
  \end{subfigure}
  \caption{Imágenes del prototipo completo del robot.}
\end{figure}

\pagebreak
\section{Modelo B}\label{sec:mecanica_modeloB}

Como modelo B se utilizó una plataforma de la empresa \emph{Freenove}
(\cite{freenove}). Se utilizó el \emph{Hexapod Robot Kit} cuya morfología es
compatible con el modelo A desarrollado. El robot está formado íntegramente por
uniones plásticas cortadas en acrílico como se puede ver en la figura
\ref{fig:freenove_full}.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.7\textwidth]{imag/png/freenove_full_raw.png}
		\caption{Imagen de cuerpo completo del modelo B.}
		\label{fig:freenove_full}
	\end{center}
\end{figure}

\subsection{Patas}

Las patas utilizan la misma morfología que la elegida para el modelo A, tres
uniones con un grado de libertad unidas con links fijos. En la figura
\ref{fig:freenove_leg} se puede ver una pata completa unida al cuerpo.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/png/freenove_leg_raw.png}
		\caption{Imagen de una pata unida al cuerpo.}
		\label{fig:freenove_leg}
	\end{center}
\end{figure}

A diferencia del modelo A el peso del cuerpo recae directamente en el eje del
servomotor dado que es la única unión entre partes. Esto no es un problema dado
que el peso total del robot es de 500g. 

La unión entre los servos está compuesta por piezas de acrílico formando 3
bloques. El primer bloque, el más cercano al cuerpo, está formado por dos
piezas de acrílico que unen dos servomotores
\ref{fig:freenove_joint1_link1_joint2}. Uno de los servos se une al cuerpo y el
otro al siguiente bloque de la pata.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/png/freenove_join1_link1_joint2_raw.png}
		\caption{Imagen del primer bloque de uniones y links que forman la pata.}
		\label{fig:freenove_joint1_link1_joint2}
	\end{center}
\end{figure}

El segundo bloque es una pieza de acrílico con dos agarres específicos para el
servo utilizado (figura \ref{fig:freenove_link2})

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/png/freenove_link2_raw.png}
		\caption{Imagen del segundo bloque que forma la pata.}
		\label{fig:freenove_link2}
	\end{center}
\end{figure}

Y el tercer bloque es el servo restante con una pieza de acrílico que hace de
último link de la cadena cinemática (figura \ref{fig:freenove_joint3_link3}).

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/png/freenove_joint3_link3_raw.png}
		\caption{Imagen del tercer bloque que forma la pata.}
		\label{fig:freenove_joint3_link3}
	\end{center}
\end{figure}

\subsection{Cuerpo}

El cuerpo está hecho de acrílico en dos partes que se sitúan de forma de
sostener el primer servo de todas las patas por ambos lados (figura \ref{fig:freenove_body}).

\begin{figure}[H]
	\begin{center}
		\includegraphics[width =0.4\textwidth]{imag/png/freenove_body_raw.png}
		\caption{Imagen del cuerpo con la electrónica.}
		\label{fig:freenove_body}
	\end{center}
\end{figure}

En el centro del cuerpo se sitúa la electrónica y las baterías del robot.
