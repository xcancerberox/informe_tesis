help:
	@echo "-------------------------------------------------------------------------"
	@echo "Las opciones son:"
	@echo "tesis: Compilar a pdf el informe de la tesis"
	@echo "images: Convertir todas las imagenes a pdf y sacarle a los .tex el center"
	@echo "check: Chequear ortografía de todos los tex de la tesis"
	@echo "biblio: Editar la bibliografía con el pybliographic"
	@echo "all: Compilar la tesis"
	@echo "-------------------------------------------------------------------------"

tesis:
	pdflatex --shell-escape tesis.tex &&\
	bibtex tesis &&\
 	pdflatex --shell-escape tesis.tex &&\
 	pdflatex --shell-escape tesis.tex

clean:
	rm tesis.pdf;\
 	rm imag/eps/*.pdf;\
 	rm imag/tex/*.pdf;\
	rm *.log *.aux *.dvi *.ps *.toc *.out *.bbl *.blg;\

images:
	cd imag/eps/ &&\
	for eps_filename in `ls *.eps`; do\
  		epstopdf $${eps_filename};\
	done;
	cd -;
	cd imag/tex/ &&\
	for eps_filename in `ls *.eps| sed -e 's/\.[^.]*$$//'`; do\
  		epstopdf $${eps_filename}.eps -o $${eps_filename}-eps-converted-to.pdf;\
	done;
	cd -

check:
	for i in `ls *.tex`; do aspell -l es --encoding=utf-8 -c $$i ;done

biblio:
	pybliographic tesis.bib

all: tesis

# Para convertir a latin1
#iconv -f utf8 -t ISO8859-1 teoysimu.tex > teoysimu.tex
# Para convertir a formato dos
#todos teoysimu.tex
