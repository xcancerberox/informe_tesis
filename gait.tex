\chapter{Modos de caminar (\emph{Gaits})} \label{cap:gaits}

%Estoy sacando de:
%- GaitStability
%- MachinesThatWalkWaldron
Un {\bf robot móvil} se diferencia de uno estanco (figura
\ref{fig:robot_movil_robot_estanco}) no solo en su capacidad de desplazar su
entorno de trabajo, sino en que en general la tarea de desplazamiento la hace
de forma autónoma, aún cuando es teledirigido el robot tiene la inteligencia
necesaria para hacer los movimientos complejos con instrucciones sencillas.

\begin{table}[H]
\begin{center}
\begin{tabular}{|c|c|}\hline
      \includegraphics[width=70mm]{imag/png/freenove_full_raw.png} &
      \includegraphics[width=50mm]{imag/png/plataforma_stewart.png} \\ \hline
\end{tabular}
    \caption{\label{fig:robot_movil_robot_estanco} A la izq. un robot movil
    caminante con 6 grados de libertad. A la derecha un robot estanco con 6
    grados de libertad.} 
\end{center}
\end{table}

Los modos de caminar son, quizá, la parte más importante en un robot caminante.
Dado que las patas no son un medio de locomoción continuo, es necesario retraer
aquellas que fueron usadas como soporte del cuerpo para que puedan ser
utilizadas posteriormente para soportarlo nuevamente. Es necesario decidir en
que posición y con que secuencia se moverán las distintas patas para lograr
que, después de una cantidad $n$ de períodos de intercambio, el robot complete
el movimiento. A esta secuencia de movimientos de las patas en grupo, que puede
ser definido con el tiempo y la posición de cada pie, en conjunto con el
movimiento del cuerpo en el espacio, con el fin de moverlo de un lugar a otro
se lo llama \emph{Gait} o {\bf Modo de andar o caminar}.

\section{Herramientas para el estudio de \emph{gaits}}

Se presentan a continuación las definiciones (\cite{MachinesThatWalkWaldron}) y
los métodos gráficos y numéricos para la representación de los \emph{Gaits} que
serán usados posteriormente para su estudio.

\subsection{Definiciones}

\begin{description}
  \item [\emph{Gait} periódico:] modo de caminar que repite su secuencia de
    movimientos periódicamente en el tiempo.
  \item [Tiempo de Ciclo:] es el tiempo que le toma a una pata completar un
    ciclo de locomoción (desde que se levanta hasta justo antes de ser
    levantada nuevamente) en un \emph{gait} periódico. Se notará como $T$.
  \item [Fase de transferencia:] es la fracción de $T$ en la que la pata no
    está apoyada.
  \item [Fase de soporte:] es la fracción de $T$ en la que la pata está
    apoyada.
  \item [Factor de trabajo:] es la fracción de tiempo de $T$ en el que una pata
    está en \emph{Fase de soporte}. Se notará como 
    \begin{eqnarray*}
        \beta_i = \frac{\text{tiempo en support phase de pata}_i}{T_i}
    \end{eqnarray*}
    donde $i$ es el número de pata, ver figura \ref{fig:gaitsreferenciastiempo}.
  \item [Fase de una pata:] es la fracción de tiempo de $T$ en el que el
    contacto con tierra de la pata $i$ está por detrás del contacto de la pata
    $1$. Se notará como $\phi_i$, ver figura
    \ref{fig:gaitsreferenciastiempo}
  \item [Fase relativa:] la fase relativa de la pata $i$ respecto de la $j$,
    $\phi_{i-j}$, es el tiempo de fase en el que el pie $i$ es apoyado antes
    que el pie $j$, ver figura \ref{fig:gaitsreferenciastiempo}. Si $j=2n-1$,
    último pie izquierdo, la fase relativa de $i$ contra $j$ se la llama fase
    negativa y se nota $\phi'_{i}$.
  \item [Fase local:] es el tiempo de fase de la posición actual del pie $i$
    hasta que es apoyado. Se nota $\psi_{i}$, ver figura
    \ref{fig:gaitsreferenciastiempo}.  Si el tiempo hasta el apoyo del pie $j$
    es $t$, se puede calcular el de $i$ como: $\psi_{i} = F(t-\phi_{i-j})$.
    \begin{figure}[ht]
      \centering
      \newsavebox{\gaitsreferenciastiempo}
      \begin{lrbox}{\gaitsreferenciastiempo}
		  	\input{imag/tex/gaitsreferenciastiempo.tex} % croped
      \end{lrbox}
      \resizebox{0.45\textwidth}{!}{\usebox{\gaitsreferenciastiempo}}
            \caption{Diagrama de \emph{gait} para el ejemplo de un robot de 6
            patas con referencia de variables temporales. (Este tipo de
            diagramas será explicado posteriormente)}
  			\label{fig:gaitsreferenciastiempo}
    \end{figure}
  \item [\emph{Leg stride}:] es la distancia que se traslada el centro de masa
    del robot en un ciclo completo de locomoción. Se notará como $\lambda$, ver
    figura \ref{fig:gaitsreferenciasespacio}.
  \item [\emph{Leg stroke}:] es la distancia que se traslada el pie relativo al
    cuerpo en la fase de soporte. Se notará como $R$, ver figura
    \ref{fig:gaitsreferenciasespacio}.
  \item [\emph{Stroke pitch}:] es la distancia entre el centro de los strokes
    de patas adyacentes de un mismo lado. Se notará como $P$, ver figura
    \ref{fig:gaitsreferenciasespacio}.
    \begin{figure}[ht]
      \centering
      \newsavebox{\gaitsreferenciasespacio}
      \begin{lrbox}{\gaitsreferenciasespacio}
		  	\input{imag/tex/gaitsreferenciasespacio.tex} % croped
      \end{lrbox}
      \resizebox{0.45\textwidth}{!}{\usebox{\gaitsreferenciasespacio}}
		    \caption{Esquema con referencias de variables espaciales de un robot de 6 patas.}
      	\label{fig:gaitsreferenciasespacio}
    \end{figure}
  \item [Matriz de un \emph{Gait}:] se notará como $G$, tendrá $n$ columnas y sus
    filas serán los estados sucesivos de la patas en un \emph{gait}, siendo el total
    de filas igual a la cantidad de estados de un ciclo de locomoción.
  \item [Fórmula de un \emph{Gait}:] es una relación entre los \emph{factores de
    trabajo} y la \emph{fase} de cada pata.  $g =
    f(\beta_1,...,\beta_n,\phi_1,...,\phi_n)$
  \item [Posición adimensional del pie:] posición del punto de contacto en el
    plano con el origen de coordenadas en el centro de gravedad. Se notará
    $(X_i,Y_i)$. La $X$ esta alineada con la dirección de translación y el $Y$
    es ortogonal.
  \item [Posición adimensional inicial de un pie:] se nota
    $(\gamma_i,\delta_i)$ y es la posición inicial al empezar con un ciclo de
    locomoción.
  \item [Formula cinemática del \emph{Gait}:] $K =
    f(\beta_i,\gamma_i,\delta_i,\phi_i)$ para $i=1,...,n$.
  \item [Evento de un \emph{Gait}:] es el levantamiento o colocación de un pie durante
    la locomoción.
  \item [\emph{Gait} singular:] un \emph{gait} es singular si dos o más eventos ocurren al
    mismo tiempo en un ciclo de locomoción.
  \item [\emph{Gait} periódico:] son aquellos \emph{gaits} que tienen un patrón que se repite
    en el tiempo. El ciclo de repetición será el tiempo de ciclo.
  \item [\emph{Gait} de fase con incremento constante:] es un \emph{gait} que tiene la misma
    diferencia de fase entre patas sucesivas de un mismo lado.
  \item [Función fraccional F(X):] $F(X) = \left\{\begin{array}{lc} \text{parte
      fraccional de} X,& \text{si} X\geq 0\\ 1 - \text{parte fraccional de}
        -X,& \text{si} X < 0\end{array}\right.$
\end{description}

\subsection{Métodos gráficos para el análisis de los \emph{Gaits}}

A continuación se presenta un conjuntos de herramientas gráficas para utilizar
en el estudio de los \emph{gaits}.

\begin{description}
  \item [Imágenes consecutivas:] usado por Muybridge (\cite{muybridge1899}) consta
    de tomar fotografías consecutivas del movimiento de animales.
  \item [Formulas pie-bola:] usado inicialmente por Hildebrand
    (\cite{Hilderbrand}) representa en una serie de gráficos la secuencia de
    pasos usando bolas llenas para indicar fase de soporte y bolas vacías para
    indicar fase de retorno (figura \ref{fig:piebolaejemplo}). Además una
    flecha indica el sentido de movimiento.
  	\begin{figure}[h]
		  \begin{center}
  			\includegraphics[width=0.5\textwidth]{imag/eps/piebolaejemplo.pdf}
            \caption{Diagrama pie-bola. Los círculos llenos indican pie
            apoyado, los vacíos pie levantado.}
	      \label{fig:piebolaejemplo}
  		\end{center}
	  \end{figure}
  \item [Diagrama de \emph{Gait}:] también usado por Hildebrand (\cite{Hilderbrand}),
    cada linea horizontal se asigna a una pata y las lineas más oscuras indican
    el período de la fase de soporte (figura \ref{fig:diagramagaitejemplo}). El
    principio de la linea oscura indica la colocación del pie en tierra y el
    fin de la linea indica el levantamiento del pie.
    \begin{figure}[h]
      \begin{center}
        \includegraphics[width=0.8\textwidth]{imag/eps/diagramagaitejemplo.pdf}
            \caption{Diagrama de \emph{Gait}, está parametrizado con el ciclo de
            trabajo.}
            \label{fig:diagramagaitejemplo}
      \end{center}
    \end{figure}
  \item [Secuencia de eventos:] es la secuencia de colocación y levantamiento
    de las patas. Para un \emph{Gait} periódico es mejor representarlo en un
    círculo (figura \ref{fig:secuenciadeeventos}).  Se lee en el sentido de las
    agujas del reloj.
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=0.3\textwidth]{imag/eps/secuenciadeeventos.pdf}
          \caption{Secuencia de eventos de \emph{Gait}, se lee en sentido horario.
          (1,2,3,4) son los eventos de apoyo de los pies y (5,6,7,8) los
          eventos de levantamiento de esos pies}
	        \label{fig:secuenciadeeventos}
		\end{center}
	\end{figure}
  \item [Patrón de soporte:] es un conjunto de puntos en un plano que
    representa el apoyo del pie durante la fase de soporte. Este punto puede ser
    pensado como el centro de presión del pie.
  \item [Patrones sucesivos:] es una serie de patrones de soporte de un \emph{Gait}.
    Se representa el cuerpo como una linea punteada y como un punto el centro
    de gravedad al inicio de la fase de soporte. La flecha indica donde termina
    el centro de gravedad al terminar esa fase de soporte (figura
    \ref{fig:patronessucesivosejemplo}).
    \begin{figure}[ht]
      \centering
      \newsavebox{\patronessucesivosejemplo}
      \begin{lrbox}{\patronessucesivosejemplo}
        \input{imag/tex/patronessucesivosejemplo.tex} % croped
      \end{lrbox}
      \resizebox{0.65\textwidth}{!}{\usebox{\patronessucesivosejemplo}}
        \caption{Patrones de soporte sucesivos de un \emph{Gait}.}
        \label{fig:patronessucesivosejemplo}
    \end{figure}
\end{description}

\section{\emph{Gaits} de caminata lineal}

El movimiento fundamental de un robot caminante es el desplazamiento en linea
recta. Existen muchos \emph{Gaits} diferentes que son adecuados para distintos
objetivos. La elección dependerá de varios factores: condición del terreno,
requerimientos de estabilidad, facilidad del control, suavidad del movimiento
del cuerpo y requerimientos de velocidad.

Lo primero entonces es hacer una distinción entre terrenos según el estado del
mismo. Para esto se divide el terreno en celdas que tienen el tamaño de la
pisada del robot. Si la celda puede ser pisada se la tilda como permitida, en
el caso que la celda no pueda ser pisada por su estado o por que es un vacío
en el terreno se la tilda como celda prohibida. Con esta definición de celdas,
si un terreno tiene más o menos celdas prohibidas se los puede dividir en 3
clases: terreno perfecto (sin celdas prohibidas en el camino elegido), terreno
razonable (algunas celdas prohibidas en el camino elegido) y terreno desigual
(algunas celdas permitidas en el camino elegido). Los \emph{Gaits} que se
presentarán a continuación serán utilizados en terrenos perfectos y planos.

\subsection{\emph{Wave Gait}}

La expresión matemática de un \emph{Wave Gait} (modo de caminar en ola) para
una máquina con $2n$ patas es:

\begin{equation}
	\phi_{2m+1} = F(m\beta), m=1,2,...,n-1 \text{ y } 1>\beta\geq\frac{3}{2n}
\end{equation}

Donde $F(X)$ es la parte fraccional del número real $X$, y $m$ son las
sucesivas patas después de la pata 1 numeradas desde adelante hacia atrás. Así
por ejemplo para $2n=4,6$ y $8$ el \emph{Gait} queda definido como:

\begin{eqnarray}
    2n = 4, m = {1}&, &\phi_3 = \beta, 1>\beta\geq\frac{3}{4}\\
    2n = 6, m = {1, 2}&, &\phi_3 = \beta, \phi_5= 2\beta-1, 1>\beta\geq\frac{1}{2}\\
    2n = 8, m = {1, 2, 3}&, &\phi_3 = \beta, \phi_5= F(2\beta), \phi_7=F(3\beta), 1>\beta\geq\frac{3}{8}
\end{eqnarray}

Esto quiere decir que para una máquina de 6 patas con un factor de trabajo de
$\beta = \frac{5}{6}$, el \emph{Gait} resultante es el que se muestra en el
figura \ref{fig:piebolawavegait}.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{imag/eps/piebolawavegait.pdf}
        \caption{Diagrama pie-bola del wave \emph{gait}.}
        \label{fig:piebolawavegait}
	\end{center}
\end{figure}

Para visualizar como queda la forma de la caminata para distintos $\beta$ se
utilizan los diagramas de \emph{Gait}.

\begin{figure}[ht]
  \centering
  \newsavebox{\diagramawavegait}
  \begin{lrbox}{\diagramawavegait}
    \input{imag/tex/diagramawavegait.tex} % croped
  \end{lrbox}
  \resizebox{0.75\textwidth}{!}{\usebox{\diagramawavegait}}
    \caption{Diagrama de \emph{Gait} para distintos factores de trabajo.}
    \label{fig:diagrama_wave_gait}
\end{figure}

En este diagrama se puede ver que dependiendo del factor de trabajo del
\emph{Gait} se pueden obtener \emph{Gaits} en los que solo una pata se
encuentra en fase de transferencia (estos son los llamados \emph{Creeping
Gaits} o modos de caminar progresivos), dos o tres patas en fase de
transferencia, o incluso tener períodos dentro del ciclo de movimiento en los
que todas las patas se encuentran en fase de soporte.

\subsection{\emph{Equal phase Gait}}

Como se vio anteriormente en el \emph{wave gait}, para ciertos factores de
trabajo, se dan eventos de levantado o apoyado de distintas patas de forma
simultanea, esto afecta el consumo pico de energía del sistema, ya que son 2 o
más las patas en movimiento.
Por ejemplo en el caso de un factor de trabajo del 50\%, para una máquina de 6
patas, a la mitad de un ciclo de locomoción se dan simultáneamente el apoyo de
3 patas y el levantado de las otras 3 patas. Esto implica un consumo pico de
corriente en ese instante que podría ser evitado.

La idea del \emph{equal phase gait} es distribuir los eventos de apoyado y
levantado de los pies a lo largo del ciclo de locomoción. Dado que este es un
\emph{gait} simétrico y regular, si los eventos de apoyo están distribuidos
equitativamente también lo estarán los de levantado, además si los eventos de
apoyo de las patas de un lado de la máquina están distribuidos equitativamente
también lo estarán las del otro lado. Entonces es suficiente asegurar que los
eventos de apoyo están distribuidos equitativamente en las patas de un lado.

Existen dos métodos de distribuir equitativamente los eventos: uno es
distribuyéndolos equitativamente en medio ciclo de locomoción (\emph{half cycle equal
phase gait}), que se extenderá el ciclo completo por la característica simétrica
del \emph{gait}; el otro es haciendo la distribución equitativa a lo largo de todo el
ciclo de locomoción (\emph{full cycle equal phase gait}). Los dos tipos de
\emph{equal phase gait} pueden ser descritos matemáticamente como se muestra a
continuación.

{\bf \emph{Half cycle equal phase gait}}
\begin{eqnarray}
	\phi_{2m+1} = 1 - \frac{m}{2n} m=1,2,...,(n-1) \label{eq:halfequalphasegait}
\end{eqnarray}

{\bf \emph{Full cycle equal phase gait}}
\begin{eqnarray}
    \phi_{2m+1} = 1 - \frac{m}{n} m=1,2,...,(n-1) \text{ con n impar} \label{eq:fullequalphasegait}
\end{eqnarray}

En la figura (\ref{fig:diagramaequalphasegait}) se puede ver el comportamiento
del \emph{gait} para distintos factores de trabajos. Si se compara con el \emph{wave
gait} para ciertos factores de trabajo, ambos \emph{gaits} se comportan de la misma
forma.

\begin{figure}[H]
    \centering
    \newsavebox{\diagramaequalphasegait}
    \begin{lrbox}{\diagramaequalphasegait}
        \input{imag/tex/diagramaequalphasegait.tex}
    \end{lrbox}
    \resizebox{0.5\textwidth}{!}{\usebox{\diagramaequalphasegait}}
    \caption{A la izq. diagrama de un \emph{half cycle equal phase gait}; A la
    der. diagrama de un \emph{full cycle equal phase gait}.}
    \label{fig:diagramaequalphasegait}
\end{figure}

\section{\emph{Gaits} de giro}

Los \emph{Gaits} de giro o \emph{Turnning gaits} son las secuencias de pasos
que le permite al robot desplazarse cambiando su orientación. Estos modos de
caminar se pueden dividir en dos categorías: \emph{Spinning Gait} o \emph{Gait}
de rotación, cuando el centro de giro del movimiento se encuentra muy cerca del
centro de simetría del robot; \emph{Circling Gait} o \emph{Gait} de giro en
círculo, cuando el centro de giro se encuentra lejos del centro de simetría del
robot.

Este tipo de movimientos se puede hacer utilizando los mismos \emph{gait} que
se usan en la caminata hacia adelante cambiando los puntos donde apoyan. Se
hará el estudio utilizando la secuencia de pasos del \emph{Wave Gait} antes
explicado.

\subsection{\emph{Spinning Gait}}

El movimiento de rotación sobre el centro de simetría del cuerpo implica que
las patas en su fase de soporte roten respecto del cuerpo en círculos centrados
en el centro de simetría. A diferencia del movimiento lineal en el cual solo se
tiene en cuenta el alcance lineal de las patas, es necesario definir el alcance
sobre el plano. Considerando una distancia máxima ($R_{MAX}$) y
mínima ($R_{MIN}$) alcanzable por el pie y un ángulo máximo ($\phi_{MAX}$) y
mínimo ($\phi_{MIN}$), el área de trabajo queda como se ve en la figura
\ref{fig:workspacespinningait}.

\begin{figure}[ht]
  \centering
  \newsavebox{\workspacespinningait}
  \begin{lrbox}{\workspacespinningait}
    \input{imag/tex/workspacespinninggait.tex} % croped
  \end{lrbox}
  \resizebox{0.4\textwidth}{!}{\usebox{\workspacespinningait}}
    \caption{Detalle del área de trabajo de una pata.}
    \label{fig:workspacespinningait}
\end{figure}

Utilizando un patrón de soporte en dos dimensiones se pueden ver la
intersección entre los círculos sobre los que deben girar los soportes
(\ref{fig:spinninggait}).

\begin{figure}[ht]
  \centering
  \newsavebox{\spinninggait}
  \begin{lrbox}{\spinninggait}
    \input{imag/tex/spinninggait.tex} % croped
  \end{lrbox}
  \resizebox{0.45\textwidth}{!}{\usebox{\spinninggait}}
    \caption{Diagrama de soporte de un \emph{Spinning Gait} para un hexapod.}
    \label{fig:spinninggait}
\end{figure}

En el caso de los \emph{Spinning Gaits} el desplazamiento del cuerpo se mide en
el ángulo girado por el cuerpo. Se define \emph{Angular Stroke} ($\Phi$) como
el ángulo rotado por el cuerpo y se calcula como el mínimo de los ángulos que
puede rotar cada pata ($\Phi_i$). Para obtener el $\Phi_i$ para cada pata se
busca la intersección entre el círculo y los bordes del espacio de trabajo.

Se definen los centros de los diagramas de soporte para cada pata ($C_i$) como
un punto sobre un círculo centrado en la base de la pata equidistante a los
radios máximo y mínimo. Se escribe este punto y la posición del centro de
simetría del cuerpo como:

\begin{eqnarray*}
  ^{p_i}\overline{C_{i}} &= (-1)^{i} \left( \Delta R_{i}, 0, 0\right)\\
  ^{p_i}\overline{CS} &= \left( -p_{ix}, -p_{iy}, 0\right)
\end{eqnarray*}

Se escriben las ecuaciones para todos los círculos y rectas indicados en la
figura \ref{fig:workspacespinningaitdetalle} en coordenadas $p_i$.

\begin{figure}[ht]
  \centering
  \newsavebox{\workspacespinningaitdetalle}
  \begin{lrbox}{\workspacespinningaitdetalle}
    \input{imag/tex/workspacespinninggaitdetalle.tex} % croped
  \end{lrbox}
  \resizebox{0.4\textwidth}{!}{\usebox{\workspacespinningaitdetalle}}
    \caption{Detalle del diagrama de soporte de un \emph{Spinning Gait}.}
    \label{fig:workspacespinningaitdetalle}
\end{figure}

\begin{eqnarray}
  C_{CS}: &\left(x + p_{ix}\right)^2 + \left(y + p_{iy}\right)^2 = r_{CS}^2\\\label{eq:ccs}
  C_{MIN}: &x^2+y^2 = R_{i_{MIN}}^2\\\label{eq:cmin}
  C_{MAX}: &x^2+y^2 = R_{i_{MAX}}^2\\\label{eq:cmax}
  L_1: &y = \tan{\phi_{MAX}} x\\\label{eq:luno}
  L_2: &y = \tan{\phi_{MIN}} x\label{eq:ldos}
\end{eqnarray}

Se buscan los puntos de intersección entre el círculo $C_{CS}$ y el resto de
las entidades indicadas. Las intersecciones son a lo sumo dos e indican el
movimiento que realizará el pie. En el caso que haya una o cero intersecciones
el movimiento no podrá realizarse.

Primero se busca la intersección con el círculo $C_{MIN}$ utilizando la
ecuación \ref{eq:cmin} para despejar $x=\pm \sqrt{R_{i_{MIN}}^2 - y^2}$ e
introducirlo en \ref{eq:ccs}.

\begin{gather}
  \left(\pm \sqrt{R_{i_{MIN}}^2-y^2} + p_{ix} \right)^2 + \left( y +p_{iy}\right)^2 = r_{CS}^2\nonumber\\
  \left(R_{i_{MIN}}^2-y^2\right)+p_{ix}^2 \pm 2 p_{ix} \sqrt{R_{i_{MIN}}^2 - y^2} + y^2 + p_{iy}^2 + 2 y p_{iy} = r_{CS}^2\nonumber\\
  2 y p_{iy} + R_{i_{MIN}}^2 + \left(p_{ix}^2 + p_{iy}^2\right) \pm 2 p_{ix} \sqrt{R_{i_{MIN}}^2 - y^2} = r_{CS}^2\nonumber\\
  2 y p_{iy} + \left(p_{ix}^2 + p_{iy}^2\right) + R_{i_{MIN}}^2 - r_{CS}^2 = \mp 2 p_{ix} \sqrt{R_{i_{MIN}}^2 - y^2}\label{eq:turngaitcuad}\\
  \left( 2 y p_{iy} + \underbrace{\left(p_{ix}^2 + p_{iy}^2\right) + R_{i_{MIN}}^2 - r_{CS}^2}_{K_{i1}} \right)^2 = \left(\mp 2 p_{ix} \sqrt{R_{i_{MIN}}^2 - y^2}\right)^2\nonumber\\
  4 y^2 p_{iy}^2 + K_{i1}^2 + 4 y p_{iy} K_{i1} = 4 p_{ix}^2 \left(R_{i_{MIN}}^2 - y^2\right)\nonumber\\
  y^2 \left( 4\left(p_{ix}^2 + p_{iy}^2 \right)\right) + y \left(4 p_{iy} K_{i1}\right) + \left(K_{i1}^2 - 4 p_{ix}^2 R_{i_{MIN}}^2 \right)= 0\nonumber
\end{gather}\\

Al elevar al cuadrado la ecuación \ref{eq:turngaitcuad} se duplican las
soluciones. Para identificar las correctas, se aplican las soluciones a la
ecuación \ref{eq:ccs} viendo que pertenezcan al círculo. Para encontrar los
puntos se resuelve la ecuación cuadrática en $y$.

\begin{eqnarray*}
  y_{1,2} &=& \frac{-b \pm \sqrt{b^2 - 4 a c}}{2a}\\
          &=&\frac{-\left(4 p_{iy} K_{i1}\right) \pm \sqrt{\left(4 p_{iy} K_{i1}\right)^2-4\left(4\left(p_{ix}^2 + p_{iy}^2 \right)\right)\left(K_{i1}^2 - 4 p_{ix}^2 R_{i_{MIN}}^2\right)}}{2\left(4\left(p_{ix}^2+p_{iy}^2\right)\right)}\\
          &=&\frac{-4 p_{iy} K_{i1} \pm 4 \sqrt{p_{iy}^2 K_{i1}^2 - p_{iy}^2 K_{i1}^2 - p_{ix}^2 K_{i1}^2 + 4 (p_{ix}^2 + p_{iy}^2) p_{ix}^2 R_{i_{MIN}}^2}}{8\left(p_{ix}^2+p_{iy}^2\right)}\\
          &=&\frac{-p_{iy} K_{i1} \pm p_{ix} \sqrt{4 (p_{ix}^2 + p_{ix}^2) R_{i_{MIN}^2} - K_{i1}^2}}{2\left(p_{ix}^2+p_{iy}^2\right)}
\end{eqnarray*}\\

Para la intersección con el círculo $C_{MAX}$ se usa el mismo procedimiento
llegando a una solución similar:

\begin{eqnarray*}
  x &=& \pm \sqrt{R_{i_{MIN}}^2 - y^2}\\
  y_{1,2} &=&\frac{-p_{iy} K_{i2} \pm p_{ix} \sqrt{4 (p_{ix}^2 + p_{ix}^2) R_{i_{MIN}^2} - K_{i2}^2}}{2\left(p_{ix}^2+p_{iy}^2\right)}\\
  k_{i2} &=& \left(p_{ix}^2 + p_{iy}^2\right) + R_{i_{MIN}}^2 - r_{CS}^2
\end{eqnarray*}\\

Utilizando la ecuación de la recta $L_1$ (\ref{eq:luno}) se reemplaza en le
ecuación del círculo \ref{eq:ccs}:

\begin{eqnarray*}
  \left( x + p_{ix}\right)^2 + \left( x \tan{\phi_{MAX}} + p_{iy}\right)^2 &=& r_{CS}^2\\
  x^2 + p_{ix}^2 + 2 p_{ix} x + x^2 \tan{\phi_{MAX}}^2 + p_{iy}^2 + 2 x \tan{\phi_{MAX}} p_{iy} &=& r_{CS}^2\\
  x^2 \left(1+\tan{\phi_{MAX}}\right) + x \left(2 p_{ix} + 2 p_{iy} \tan{\phi_{MAX}}\right) + \left[\left(p_{ix}^2+p_{iy}^2\right)-r_{CS}^2\right] &=& 0
\end{eqnarray*}\\

Se resuelve nuevamente la ecuación cuadrática en $x$:
\begin{eqnarray*}
  x_{1,2} &=& \frac{-2 \left( p_{ix} + p_{iy} \tan{\phi_{MAX}}\right) \pm \sqrt{4\left( p_{ix} + p_{iy} \tan{\phi_{MAX}}\right)^2 - \frac{4\left[\left(p_{ix}^2 + p_{iy}^2\right)-r_{CS}^2\right]}{\coslargo{\phi_{MAX}}^2}}}{2\left(1+\tan{\phi_{MAX}}^2\right)}\\
          &=& \coslargo{\phi_{MAX}}^2 \left[-\left( p_{ix} + p_{iy} \tan{\phi_{MAX}}\right) \pm \sqrt{\left( p_{ix} + p_{iy} \tan{\phi_{MAX}}\right)^2 - \frac{\left[\left(p_{ix}^2 + p_{iy}^2\right)-r_{CS}^2\right]}{\coslargo{\phi_{MAX}}^2}}\right]
\end{eqnarray*}\\

De la misma forma utilizando la ecuación \ref{eq:ldos} y \ref{eq:ccs} se llega a la solución para $L_2$:
\begin{eqnarray*}
  x_{1,2} &=& \coslargo{\phi_{MIN}}^2 \left[-\left( p_{ix} + p_{iy} \tan{\phi_{MIN}}\right) \pm \sqrt{\left( p_{ix} + p_{iy} \tan{\phi_{MIN}}\right)^2 - \frac{\left[\left(p_{ix}^2 + p_{iy}^2\right)-r_{CS}^2\right]}{\coslargo{\phi_{MIN}}^2}}\right]
\end{eqnarray*}\\

Para descartar las soluciones que no son del límite del área de trabajo de la
pata se hacen cumplir las siguientes restricciones, según la figura
\ref{fig:workspacespinningaitdetalle}:

\begin{eqnarray*}
  \phi_{MIN} \leq &\phi& \leq \phi_{MAX}\\
  R_{MIN} \leq &r& \leq R_{MAX}
\end{eqnarray*}

Por simetría del cuerpo $\Phi_1 = \Phi_2 = \Phi_5 = \Phi_6$ y $\Phi_3 =
\Phi_4$, por lo tanto para una fase de soporte tomo uno de cada grupo y $\Phi =
\min{(\Phi_1, \Phi_3)}$ obteniéndolas como la diferencia entre los ángulos de las
intersecciones previamente calculadas para cada una de esas patas.

Para la elección de que patas están en soporte y cuales en transferencia se
puede utilizar cualquiera de los \emph{gaits} presentados anteriormente.
