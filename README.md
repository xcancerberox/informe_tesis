Compilacion del informe con docker
----------------------------------

Generar imagen docker

```
informe_tesis$ cd docker
informe_tesis/docker$ docker build -t pdflatex .
```

Para compilar el informe con el docker ya creado
```
informe_tesis$ docker run --rm -v ${PWD}:/srv/build -it pdflatex make images
informe_tesis$ docker run --rm -v ${PWD}:/srv/build -it pdflatex make tesis 
```

Para limpiar el directorio
```
informe_tesis$ docker run --rm -v ${PWD}:/srv/build -it pdflatex make clean 
```
